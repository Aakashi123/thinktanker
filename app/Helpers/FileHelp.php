<?php
namespace App\Helpers;

class FileHelp
{
	public static function getfilename($file){

        $ext = last(explode('.',$file->getClientOriginalName()));
        $microtime       = microtime();
        $search          = array('.',' ');
        $microtime       = str_replace($search, "_", $microtime);
        $fileName        = sha1($microtime).".".$ext;
        // dd($fileName);

        return $fileName;
    }

     public static function UnlinkImage($filepath,$fileName){
        $old_image = $filepath.$fileName;
    	// dd($old_image);
        if (file_exists($old_image)) {
            @unlink($old_image);
        }
    }
}