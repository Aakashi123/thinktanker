<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Model\Projectmaster;
use Illuminate\Http\Request;
use App\Helpers\FileHelp;

class ProjectmasterJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $projectmaster_data;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($projectmaster_data)
    {
        $this->projectmaster_data =$projectmaster_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $projectmaster_data= $this->projectmaster_data;
        $id= $request->id;
        if(isset($projectmaster_data['id']))
        {
            $id = $projectmaster_data['id'];  
        }

        $save_detail =Projectmaster::firstOrNew(['id' => $id]);
        $save_detail->fill($projectmaster_data);

        if(!empty($request->slug))
        {
            $save_detail->slug = $save_detail->slug;
        }
        else
        {  
            $save_detail->slug = str_slug($save_detail->title);
        }
        
        $old_image = Projectmaster::where('id',$id)->value('image');        
        $path = public_path().'/images/projectmaster/';
        
        if ($request->hasfile('image')) 
        {
            if ($old_image != null) {

                FileHelp::UnlinkImage($path, $old_image);

            }
            $file = $request->file('image');

            $filename = FileHelp::getfilename($file);

            $request->file('image')->move($path, $filename);

            $save_detail->image = $filename;

        }
        $save_detail->save();        
        return;
    }
}
