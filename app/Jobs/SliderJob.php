<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Model\Slider;
use Illuminate\Http\Request;
use App\Helpers\FileHelp;
class SliderJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $slider_data;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($slider_data)
    {

        $this->slider_data =$slider_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {

        $slider_data= $this->slider_data;
        // dd($slider_data);
        $id= $request->id;
        if(isset($slider_data['id']))
        {
            $id = $slider_data['id'];  
        }

        $save_detail =Slider::firstOrNew(['id' => $id]);
        $save_detail->fill($slider_data);
        
        $old_image = Slider::where('id',$id)->value('image');        
        $path = public_path().'/images/slider/';
        
        if ($request->hasfile('image')) 
        {
            if ($old_image != null) {

                FileHelp::UnlinkImage($path, $old_image);

            }
            $file = $request->file('image');

            $filename = FileHelp::getfilename($file);

            $request->file('image')->move($path, $filename);

            $save_detail->image = $filename;

        }
        $save_detail->save();        
        return;
    }
}
