<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Model\Generalinfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GeneralinfoJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $generalinfo_data;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($generalinfo_data)
    {
        $this->generalinfo_data =$generalinfo_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $generalinfo_data= $this->generalinfo_data;
        $id= $request->id;
        if(isset($generalinfo_data['id']))
        {
            $id = $generalinfo_data['id'];  
        }

        $save_detail =Generalinfo::firstOrNew(['id' => $id]);
        $save_detail->fill($generalinfo_data);

        $admin_id = auth()->guard('admin')->id();
        $save_detail->save();
    }
}
