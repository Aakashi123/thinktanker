<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Model\Career;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CareerJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $career_data;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($career_data)
    {
        $this->career_data =$career_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
       $career_data= $this->career_data;
        $id= $request->id;
        if(isset($career_data['id']))
        {
            $id = $career_data['id'];  
        }

        $save_detail =Career::firstOrNew(['id' => $id]);
        $save_detail->fill($career_data);

              
        if(!empty($request->slug))
        {
            $save_detail->slug = $save_detail->slug;
        }
        else
        {  
            $save_detail->slug = str_slug($save_detail->title);
        }
        $save_detail->save();
        
        return; 
    }
}
