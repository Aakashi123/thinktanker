<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Model\Clientlogo;
use Illuminate\Http\Request;
use App\Helpers\FileHelp;
use Illuminate\Support\Facades\Auth;

class ClientlogoJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($clientlogo_data)
    {
        $this->clientlogo_data =$clientlogo_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $clientlogo_data= $this->clientlogo_data;

        $files = $request->only('files');
        if (!empty($files)) 
        {
            $image = $files['files'];
            if (isset($image)) {
                foreach ($image as $key => $image_array) 
                {
                    $image_save = new Clientlogo();
                    if ($request->hasfile('files')) 
                    {
                        $filename1 = FileHelp::getfilename($image_array);
                        $image_array->move(public_path() . '/images/clientlogo/', $filename1);
                            $image_save->image_name = $filename1;                      
                    }
                    $image_save->save();
                }
            }
        }
        return;
    }
}
