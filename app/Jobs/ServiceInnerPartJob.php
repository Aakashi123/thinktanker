<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Model\ServiceInnerpart;
use Illuminate\Http\Request;
use App\Helpers\FileHelp;

class ServiceInnerPartJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $servicepart_data;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($servicepart_data)
    {
        $this->servicepart_data =$servicepart_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $servicepart_data= $this->servicepart_data;
        $id= $request->id;
        if(isset($servicepart_data['id']))
        {
            $id = $servicepart_data['id'];  
        }

        $save_detail =ServiceInnerpart::firstOrNew(['id' => $id]);
        $save_detail->fill($servicepart_data);

        $old_image = ServiceInnerpart::where('id',$id)->value('inner_image');        
        $path = public_path().'/images/service_inner/';
        
        if ($request->hasfile('inner_image')) 
        {
            if ($old_image != null) {

                FileHelp::UnlinkImage($path, $old_image);

            }
            $file = $request->file('inner_image');

            $filename = FileHelp::getfilename($file);

            $request->file('inner_image')->move($path, $filename);

            $save_detail->inner_image = $filename;

        }
        $save_detail->save();        
        return;
    }
}
