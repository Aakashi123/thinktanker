<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Model\Testimonial;
use Illuminate\Http\Request;
use App\Helpers\FileHelp;

class TestimonialJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $testimonial_data;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($testimonial_data)
    {
        $this->testimonial_data =$testimonial_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $testimonial_data= $this->testimonial_data;
        $id= $request->id;
        if(isset($testimonial_data['id']))
        {
            $id = $testimonial_data['id'];  
        }

        $save_detail =Testimonial::firstOrNew(['id' => $id]);
        $save_detail->fill($testimonial_data);
        
        $old_image = Testimonial::where('id',$id)->value('image');        
        $path = public_path().'/images/testimonial/';
        
        if ($request->hasfile('image')) 
        {
            if ($old_image != null) {

                FileHelp::UnlinkImage($path, $old_image);

            }
            $file = $request->file('image');

            $filename = FileHelp::getfilename($file);

            $request->file('image')->move($path, $filename);

            $save_detail->image = $filename;

        }
        $save_detail->save();        
        return;
    }
}
