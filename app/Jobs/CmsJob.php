<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Model\Cms;
use Illuminate\Http\Request;
use App\Helpers\FileHelp;

class CmsJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $cms_data;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($cms_data)
    {
        $this->cms_data =$cms_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $cms_data= $this->cms_data;
        $id= $request->id;
        if(isset($cms_data['id']))
        {
            $id = $cms_data['id'];  
        }

        $save_detail =Cms::firstOrNew(['id' => $id]);
        $save_detail->fill($cms_data);

        if(!empty($request->slug))
        {
            $save_detail->slug = $save_detail->slug;
        }
        else
        {  
            $save_detail->slug = str_slug($save_detail->title);
        }
        
        $old_image = Cms::where('id',$id)->value('image');        
        $path = public_path().'/images/cms/';
        
        if ($request->hasfile('image')) 
        {
            if ($old_image != null) {

                FileHelp::UnlinkImage($path, $old_image);

            }
            $file = $request->file('image');

            $filename = FileHelp::getfilename($file);

            $request->file('image')->move($path, $filename);

            $save_detail->image = $filename;

        }
        $save_detail->save();        
        return;
    }
}
