<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Model\Servicemaster;
use Illuminate\Http\Request;
use App\Helpers\FileHelp;

class ServicemasterJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $servicemaster_data;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($servicemaster_data)
    {
        $this->servicemaster_data =$servicemaster_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $servicemaster_data= $this->servicemaster_data;
        $id= $request->id;
        if(isset($servicemaster_data['id']))
        {
            $id = $servicemaster_data['id'];  
        }

        $save_detail =Servicemaster::firstOrNew(['id' => $id]);
        $save_detail->fill($servicemaster_data);

        if(!empty($request->slug))
        {
            $save_detail->slug = $save_detail->slug;
        }
        else
        {  
            $save_detail->slug = str_slug($save_detail->home_title);
        }
        
        $old_image = Servicemaster::where('id',$id)->value('image');        
        $path = public_path().'/images/servicemaster/';
        
        if ($request->hasfile('image')) 
        {
            if ($old_image != null) {

                FileHelp::UnlinkImage($path, $old_image);

            }
            $file = $request->file('image');

            $filename = FileHelp::getfilename($file);

            $request->file('image')->move($path, $filename);

            $save_detail->image = $filename;

        }
        $save_detail->save();        
        return;
    }
}
