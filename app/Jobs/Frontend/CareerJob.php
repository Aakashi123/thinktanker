<?php

namespace App\Jobs\Frontend;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Model\Frontend\Career;
use Illuminate\Http\Request;
use App\Helpers\FileHelp;

class CareerJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $career_data;
    protected $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($career_data)
    {
        $this->career_data =$career_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $career_data= $this->career_data;

        $id= $request->id;
        
        if(isset($career_data['id']))
        {
            $id = $career_data['id'];  
        }

        $save_detail =Career::firstOrNew(['id' => $id]);

        $save_detail->fill($career_data);   

        if(!empty($career_data['resume_name']))
        {
            $file = $request->file('resume_name');
           
              $filename = time()."_".$file->getClientOriginalName(); 

              $destinationpath = public_path().'/files/';
              
              if(!file_exists($destinationpath))
              {
                  mkdir($destinationpath,0777);
              }
              $file->move($destinationpath,$filename);

              $save_detail->resume_name =$filename;
              
              $save_detail->save();
            
        }
        
        return;
    }
}
