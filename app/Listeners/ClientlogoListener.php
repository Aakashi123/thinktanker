<?php

namespace App\Listeners;

use App\Events\ClientlogoEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\ClientlogoJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ClientlogoListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ClientlogoEvent  $event
     * @return void
     */
    public function handle(ClientlogoEvent $event)
    {
        $clientlogo_data = $event->clientlogo_data;
        $save_detail= $this->dispatch(new ClientlogoJob($clientlogo_data));
    }
}
