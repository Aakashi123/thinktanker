<?php

namespace App\Listeners;

use App\Events\SliderEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\SliderJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class SliderListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SliderEvent  $event
     * @return void
     */
    public function handle(SliderEvent $event)
    {
        $slider_data = $event->slider_data;
        $save_detail= $this->dispatch(new SliderJob($slider_data));
    }
}
