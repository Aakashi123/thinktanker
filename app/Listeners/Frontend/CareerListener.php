<?php

namespace App\Listeners\Frontend;

use App\Events\Frontend\CareerEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\Frontend\CareerJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class CareerListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Frontend-CareerEvent  $event
     * @return void
     */
    public function handle(CareerEvent $event)
    {
        $career_data = $event->career_data;
        $save_detail= $this->dispatch(new CareerJob($career_data));
    }
}
