<?php

namespace App\Listeners;

use App\Events\CmsEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\CmsJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class CmsListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CmsEvent  $event
     * @return void
     */
    public function handle(CmsEvent $event)
    {
        $cms_data = $event->cms_data;
        $save_detail= $this->dispatch(new CmsJob($cms_data));
    }
}
