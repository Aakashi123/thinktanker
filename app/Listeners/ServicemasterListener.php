<?php

namespace App\Listeners;

use App\Events\ServicemasterEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\ServicemasterJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ServicemasterListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ServicemasterEvent  $event
     * @return void
     */
    public function handle(ServicemasterEvent $event)
    {
        $servicemaster_data = $event->servicemaster_data;
        $save_detail= $this->dispatch(new ServicemasterJob($servicemaster_data));
    }
}
