<?php

namespace App\Listeners;

use App\Events\Frontend-CareerEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class Frontend-CareerListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Frontend-CareerEvent  $event
     * @return void
     */
    public function handle(Frontend-CareerEvent $event)
    {
        //
    }
}
