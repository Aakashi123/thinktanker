<?php

namespace App\Listeners;

use App\Events\GeneralinfoEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\GeneralinfoJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class GeneralinfoListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GeneralinfoEvent  $event
     * @return void
     */
    public function handle(GeneralinfoEvent $event)
    {
        $generalinfo_data = $event->generalinfo_data;
        $save_detail= $this->dispatch(new GeneralinfoJob($generalinfo_data));
    }
}
