<?php

namespace App\Listeners;

use App\Events\TestimonialEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\TestimonialJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class TestimonialListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TestimonialEvent  $event
     * @return void
     */
    public function handle(TestimonialEvent $event)
    {
        $testimonial_data = $event->testimonial_data;
        $save_detail= $this->dispatch(new TestimonialJob($testimonial_data));
    }
}
