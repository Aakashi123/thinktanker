<?php

namespace App\Listeners;

use App\Events\ProjectmasterEvent;
use Illuminate\Queue\InteractsWithQueue;
use App\Jobs\ProjectmasterJob;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ProjectmasterListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProjectmasterEvent  $event
     * @return void
     */
    public function handle(ProjectmasterEvent $event)
    {
        $projectmaster_data = $event->projectmaster_data;
        $save_detail= $this->dispatch(new ProjectmasterJob($projectmaster_data));
    }
}
