<?php

namespace App\Listeners;

use App\Events\ServiceInnerpartEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\ServiceInnerPartJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ServiceInnerpartListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ServiceInnerpartEvent  $event
     * @return void
     */
    public function handle(ServiceInnerpartEvent $event)
    {
        $servicepart_data = $event->servicepart_data;
        $save_detail= $this->dispatch(new ServiceInnerPartJob($servicepart_data));
    }
}
