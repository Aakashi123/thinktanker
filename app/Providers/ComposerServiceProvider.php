<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Model\MenuItem;
use App\Model\Menu;
use App\Model\Generalinfo;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

         view()->composer('*', function ($view) {

            $menuitems = MenuItem::select('id','label','link','parent')->with('children.subchildren')->where('parent','=',0)->get();

            $general_data = Generalinfo::select('google','facebook','linkedin','twitter','instagram','youtube','title','subtitle','email','mobile1','mobile2','address','meta_title','meta_description','copyright','meta_keyword','time','address1')->first();

            $view->with(['menuitems'=>$menuitems,'general_data'=>$general_data]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
