<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],

        'App\Events\SliderEvent' => [
            'App\Listeners\SliderListener',
        ],

        'App\Events\TestimonialEvent' => [
            'App\Listeners\TestimonialListener',
        ],

        'App\Events\ProjectmasterEvent' => [
            'App\Listeners\ProjectmasterListener',
        ],

        'App\Events\CmsEvent' => [
            'App\Listeners\CmsListener',
        ],

        'App\Events\ClientlogoEvent' => [
            'App\Listeners\ClientlogoListener',
        ],

        'App\Events\ServicemasterEvent' => [
            'App\Listeners\ServicemasterListener',
        ],

        'App\Events\ServiceInnerpartEvent' => [
            'App\Listeners\ServiceInnerpartListener',
        ],


        'App\Events\CareerEvent' => [
            'App\Listeners\CareerListener',
        ],

        'App\Events\GeneralinfoEvent' => [
            'App\Listeners\GeneralinfoListener',
        ],

        'App\Events\Frontend\CareerEvent' => [
            'App\Listeners\Frontend\CareerListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
