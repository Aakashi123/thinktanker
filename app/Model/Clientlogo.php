<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Clientlogo extends Model
{
    protected $table = "clientlogos";
    protected $fillable = ['iamge_name'];
}
