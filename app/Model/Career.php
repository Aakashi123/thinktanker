<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    protected $table ="careers";
    protected $fillable = ['title','slug','activetitle','description','metatitle','metadescription','metakeyword'];

    public function settitleAttribute($title)
    {
    	$set_title = ucfirst($title);
    	return $this->attributes['title'] = $set_title;
    }
}
