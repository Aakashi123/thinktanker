<?php

namespace App\Model\Frontend;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $table = "contact-us";
    protected $fillable = ['fname','email','contact','about_enquiry','message'];
}
