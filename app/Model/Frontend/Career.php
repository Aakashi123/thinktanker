<?php

namespace App\Model\Frontend;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    protected $table = "frontend_careers";
    protected $fillable = ['id','position','fname','birthdate','email','contact','address','educational','resume_name'];
}
