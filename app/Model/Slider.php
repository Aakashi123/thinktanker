<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = "sliders";
    protected $fillable = ['title','enabletitle','subtitle','enablesubtitle','position','description','image','link'];

    public function settitleAttribute($title)
    {
    	$set_title = ucfirst($title);
    	return $this->attributes['title'] = $set_title;
    }
}
