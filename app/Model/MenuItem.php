<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    protected $table="menu_items";
    protected $fillable=['label','link','parent','sort','class','menu','depth'];

    public function parent()
 	{
 		return $this->belongsTo('App\Model\Menu','parent');
 	}
 	public function children()
 	{
 		return $this->hasMany('App\Model\MenuItem','parent');
 	}
 	public function subchildren()
 	{
 		return $this->hasMany('App\Model\MenuItem','parent','id');
 	}
}
