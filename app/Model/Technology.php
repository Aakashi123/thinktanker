<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Technology extends Model
{
    protected $table = "technologys";
    protected $fillable = ['id','technology_name'];
}
