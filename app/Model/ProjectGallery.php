<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProjectGallery extends Model
{
    protected $table = "projectgallerys";
    protected $fillable = ['id','title_id','image'];
}
