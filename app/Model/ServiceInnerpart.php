<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServiceInnerpart extends Model
{
    protected $table = "service_innerparts";
    protected $fillable = ['title','description','services','metatitle','metadescription','metakeyword','inner_image'];

    public function settitleAttribute($title)
    {
    	$set_title = ucfirst($title);
    	return $this->attributes['title'] = $set_title;
    }
}
