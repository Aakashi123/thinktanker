<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Generalinfo extends Model
{
    protected $table = "generalinfos";
    protected $fillable = ['google','facebook','linkedin','twitter','instagram','youtube','title','subtitle','email','mobile1','mobile2','address','meta_title','meta_description','copyright','meta_keyword','time','address1'];

    public function settitleAttribute($title)
    {
    	$set_title = ucfirst($title);
    	return $this->attributes['title'] = $set_title;
    }
}
