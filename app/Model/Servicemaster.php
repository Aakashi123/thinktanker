<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Servicemaster extends Model
{
    protected $table = "servicemasters";
    protected $fillable = ['home_title','metatitle','slug','home_description','metadescription','metakeyword','image','inner_title','inner_description'];

    public function settitleAttribute($home_title)
    {
    	$set_hometitle = ucfirst($home_title);
    	return $this->attributes['home_title'] = $set_hometitle;
    }

    // public function setinnertitleAttribute($inner_title)
    // {
    // 	$set_innertitle = ucfirst($inner_title)
    // 	return $this->attributes['inner_title'] = $set_innertitle;
    // }
}
