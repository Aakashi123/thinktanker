<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProjectTitle extends Model
{
    protected $table = "projecttitles";
    protected $fillable = ['id','title'];
}
