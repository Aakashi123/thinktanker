<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Educational extends Model
{
    protected $table = "educationals";
    protected $fillable = ['educational_name'];
}
