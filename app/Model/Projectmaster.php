<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Projectmaster extends Model
{
    protected $table = "projectmasters";
    protected $fillable = ['title','slug','metatitle','metadescription','description','image','link','metakeyword'];

    public function settitleAttribute($title)
    {
    	$set_title = ucfirst($title);
    	return $this->attributes['title'] = $set_title;
    }
}
