<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $table = "testimonials";
    protected $fillable = ['name','description','image'];

    public function settitleAttribute($name)
    {
    	$set_name = ucfirst($name);
    	return $this->attributes['name'] = $set_name;
    }
}
