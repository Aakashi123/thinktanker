<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServicemasterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
            {
                return [
                'home_title' => 'required|max:50',
                'home_description' => 'required',
                'inner_title' => 'required|max:50',
                'inner_description' => 'required',
                'image' => 'required|mimes:jpeg,png|dimensions:width=200,height=200',
                ]; 
            }
            case 'PATCH':
            {
                return [
                'home_title' => 'required|max:50',
                'home_description' => 'required',
                'inner_title' => 'required|max:50',
                'inner_description' => 'required',
                'image' => 'nullable|mimes:jpeg,png|dimensions:width=200,height=200',
                ]; 
            }
            default : break;
        }
    }

    public function messages()
    {
        return[
           'home_title.required' => 'The home-page title field is required.',
            'home_description.required' => 'The home-page description field is required.',
            'inner_title.required' => 'The inner-page title field is required.',
            'inner_description.required' => 'The inner-page description field is required.',
        ];
    }
}
