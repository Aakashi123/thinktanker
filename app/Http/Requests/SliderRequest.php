<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
            {
                return [
                'title' => 'required|regex:'.config('regex.slider.title').'|max:50',
                'subtitle' => 'required|max:100',
                'description' => 'required',
                'image' => 'required|mimes:jpeg,png|dimensions:width=200,height=200',
                'link' => 'nullable|url',
                ]; 
            }
            case 'PATCH':
            {
                return [
                'title' => 'required|regex:'.config('regex.slider.title').'|max:50',
                'subtitle' => 'required|max:100',
                'description' => 'required',
                'image' => 'nullable|mimes:jpeg,png|dimensions:width=200,height=200',
                'link' => 'nullable|url',
                ]; 
            }
            default : break;
        }
    }

    public function messages(){
        return [
            'subtitle.required' => 'The sub title field is required.'
        ];
    }
}
