<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class ContactUsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname' => 'required',
            'email' => 'required|email',
            'contact' => 'required|min:10|max:10',
            'message' => 'required',
        ];
    }

    public function messages()
    {
        return[

            'fname.required' => 'Please enter full name',
            'email.required' => 'Please enetr email address',
            'email.email' => 'Please enter email proper format',
            'contact.required' => 'Please enter contact number',
            'contact.numeric' => 'Only contains digits',
            'message.required' => 'Please enter message',
        ];
    }
}
