<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class CareerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname' => 'required|alpha',
            'birthdate' => 'required|before:now',
            'position' => 'required',
            'educational' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'contact' => 'required|min:10|max:10',
            'resume_name' => 'required|mimes:pdf',
        ];
    }

    public function messages()
    {
        return [
            'fname' => 'The First Name field is required.',
            'fname' => 'The Firest Name may only contain letters.',
            'birthdate' => 'The birthdate field is required.',
            'position' => 'The Position field is required.',
            'educational' => 'The Educational Background field is required',
            'contact' => 'The Contact number field is required.',
            'resume_name.required' => 'Please Upload your resume',
            'resume_name.mimes' => 'The resume must be a file of type: pdf.',
        ];
    }
}
