<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TestimonialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         switch ($this->method()) {

            case 'POST':
            {
                return [
                'name' => 'required|max:30|regex:'.config('regex.testimonial.name'),
                'description' => 'required',
                'image' => 'required|mimes:jpeg,png|dimensions:width=100,height=100',
                ]; 
            }
            case 'PATCH':
            {
                return [
                'name' => 'required|max:30|regex:'.config('regex.testimonial.name'),
                'description' => 'required',
                'image' => 'nullable|mimes:jpeg,png|dimensions:width=100,height=100',
                ]; 
            }
            default : break;
        }
    }
    public function messages()
    {
        return[
            'name.required' => 'The client name field is required.',
            'name.regex' => 'The client name format is invalid.',
            'image.required' => 'The client profile is required.',
            'image.mimes' => 'The client profile must be a file of type:jpeg,png.',
            'image.dimensions' => 'The client profile has invalid image dimensions.'
        ];
    }
}
