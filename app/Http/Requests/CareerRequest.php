<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CareerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
            {
                return [
                'title' => 'required|max:50|regex:'.config('regex.career.title'),
                'description' => 'required',
                'slug' => 'nullable|unique:careers,slug|regex:'.config('regex.slug.slug'),
                ]; 
            }
            case 'PATCH':
            {
                return [
                'title' => 'required|max:50|regex:'.config('regex.career.title'),
                'description' => 'required',
                'slug' => 'nullable|unique:careers,slug,'.$this->segment(3).'|regex:'.config('regex.slug.slug'),
                ]; 
            }
            default : break;
        }
    }
}
