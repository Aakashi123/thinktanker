<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CmsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
            {
                return [
                'title' => 'required|max:50|regex:'.config('regex.cms.title').'|unique:cms,title',
                'description' => 'required',
                'image' => 'nullable|mimes:jpeg,png|dimensions:width=200,height=200',
                'link' => 'nullable|url',
                'slug' => 'nullable|unique:cms,slug|regex:'.config('regex.slug.slug'),
                ]; 
            }
            case 'PATCH':
            {
                return [
                'title' => 'required|max:50|regex:'.config('regex.cms.title').'|unique:cms,title,'.$this->segment(3),
                'description' => 'required',
                'image' => 'nullable|mimes:jpeg,png|dimensions:width=200,height=200',
                'link' => 'nullable|url',
                'slug' => 'nullable|unique:cms,slug,'.$this->segment(3).'|regex:'.config('regex.slug.slug'),
                ]; 
            }
            default : break;
        }
    }
}
