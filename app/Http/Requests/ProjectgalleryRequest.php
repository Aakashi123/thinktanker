<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectgalleryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
            {
                return [
                'title' => 'required|max:50|regex:'.config('regex.project.title'),
                'image' => 'required',
                ]; 
            }
            case 'PATCH':
            {
                return [
                'title' => 'required|max:50|regex:'.config('regex.project.title'),
                'image' => 'nullable',
                ]; 
            }
            default : break;
        }
    }

    public function messages()
    {
        return [
            'title.regex' => 'The project gallery title format is invalid.', 
        ];
    }
}
