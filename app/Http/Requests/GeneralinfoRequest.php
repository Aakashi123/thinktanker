<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GeneralinfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'google' => 'nullable|url',
        'facebook' => 'nullable|url',
        'twitter' => 'nullable|url',
        'linkedin' => 'nullable|url',
        'instagram' => 'nullable|url',
        'youtube' => 'nullable|url',
        'title' => 'required|regex:'.config('regex.generalinfo.title').'|max:50|alpha',
        'subtitle' => 'nullable|regex:'.config('regex.generalinfo.subtitle').'|max:100|alpha',
        'email' => 'required|email',
        'mobile1' => 'required|regex:'.config('regex.generalinfo.mobile1').'|min:10|max:10',
        'mobile2' => 'required|regex:'.config('regex.generalinfo.mobile2').'|min:10|max:10',
        'address' => 'required|max:100',
        'address1' => 'required|max:100',
        'copyright' => 'required',
        'time' => 'required'
        ];
    }

    public function messages()
    {
        return[
            'mobile1.required' => 'The contactNo field is required.',
            'mobile1.min' => 'The contactNo must be atleast 10 characters.',
            'mobile1.max' => 'The contactNo must be atleast 10 characters.',
            'mobile1.regex' => 'The contactNo format is invalid.',
            'mobile2.min' => 'The Alternate contactNo must be atleast 10 characters.',
            'mobile2.min' => 'The Alternate contactNo must be atleast 10 characters.',
            'mobile2.regex' => 'The Alternate contactNo format is invalid.',
            'mobile2.required' => 'The Alternate contactNo field is required.',
            'email.required' => 'The email id field is required.',
            'email.email' => 'The email id must be a valid email address.',
            'time.required' => 'The timing detail field is required.',
            'address.required' => 'The ahmedabad address field is required.',
            'address1.required' => 'The mumbai address field is required.',
        ];
    }
}
