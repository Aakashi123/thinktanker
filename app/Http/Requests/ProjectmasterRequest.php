<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectmasterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
            {
                return [
                'title' => 'required|max:50|regex:'.config('regex.project.title').'|unique:projectmasters,title',
                'description' => 'required',
                'image' => 'required|mimes:jpeg,png|dimensions:width=200,height=200',
                'link' => 'required|url',
                'slug' => 'nullable|unique:projectmasters,slug|regex:'.config('regex.slug.slug'),
                ]; 
            }
            case 'PATCH':
            {
                return [
                'title' => 'required|max:50|regex:'.config('regex.project.title').'|unique:projectmasters,title,'.$this->segment(3),
                'description' => 'required',
                'image' => 'nullable|mimes:jpeg,png|dimensions:width=200,height=200',
                'link' => 'required|url',
                'slug' => 'nullable|unique:projectmasters,slug,'.$this->segment(3).'|regex:'.config('regex.slug.slug'),
                ]; 
            }
            default : break;
        }
    }

    public function messages(){
        return [
            'title.required' => 'The project title field is required.',
            'title.regex' => 'The project title format is invalid.',
            'title.unique' => 'The project title has already been taken.',
            'description.required' => 'The project description is required.'
        ];
    }
}
