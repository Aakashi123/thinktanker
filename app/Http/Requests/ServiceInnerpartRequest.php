<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceInnerpartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
            {
                return [
                'title' => 'required|max:50',
                'description' => 'required',
                'services' => 'required',
                'inner_image' => 'required|dimensions:width=100,height=100'
                ]; 
            }
            case 'PATCH':
            {
                return [
                'title' => 'required|max:50',
                'description' => 'required',
                'services' => 'required',
                'inner_image' => 'nullable|dimensions:width=100,height=100'
                ];
            }
            default : break;
        }
    }
}
