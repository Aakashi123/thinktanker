<?php

namespace App\Http\ViewComposer;

use Illuminate\View\View;
use App\Model\MenuItem;
use Illuminate\Http\Request;

class SetActiveMenuComposer
{
    private $menu_hierarchy;
    private function getMenuHierArchy($menuItemAID)
    {

        $slugObj = MenuItem::find($menuItemAID);
        
        $this->menu_hierarchy[$slugObj->id] = $slugObj->toArray();

        if($slugObj->parent != '0') $this->getMenuHierArchy($slugObj->parent);
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {   
        // $slug = \Request::segment(1);
        // echo $slug;
        $slugObj = MenuItem::select('id','label','link','parent')->where('link',$slug)->first();

        $view->with('set_active_menu_items', $this->menu_hierarchy);
    }
}