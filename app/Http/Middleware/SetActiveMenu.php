<?php

namespace App\Http\Middleware;

use Closure;
use View;
use App\Model\MenuItem;
class SetActiveMenu{
	
	private $menu_hierarchy;
    private function getMenuHierArchy($menuItemAID)
    {

        $slugObj = MenuItem::find($menuItemAID);
        
        $this->menu_hierarchy[$slugObj->id] = $slugObj->toArray();

        if($slugObj->parent != '0') $this->getMenuHierArchy($slugObj->parent);
    }

	public function handle($request, Closure $next)
    {
		
		$slug = $request->segment(1);

        if($slug == '')
        {
            $slug = 'homepage';
        }
        $slugObj = MenuItem::select('id','label','parent','link')->where('link',$slug)->first();
        if($slugObj){
            
            $this->getMenuHierArchy($slugObj->id);
        
            View::share('set_active_menu_items', $this->menu_hierarchy);
        }
        
		return $next($request);
	}
}
