<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Career;
use Event;
use App\Events\CareerEvent;
use App\Http\Requests\CareerRequest;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd(Career::all());
        if ($request->ajax()) {
            $where_paramsstr = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_paramsstr .= " and ( title like \"%{$search}%\")";
            }
            $user = Career::select('id','title','activetitle')
            ->whereRaw($where_paramsstr, $where_params);

            $user_count = Career::select('id')
            ->whereRaw($where_paramsstr, $where_params)
            ->count();

            $columns = ['id','title','activetitle'];

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $user = $user->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $user = $user->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            $user = $user->get();
            $response['iTotalDisplayRecords'] = $user_count;
            $response['iTotalRecords'] = $user_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $user;

            return $response;
        }
        return view('admin.career.careermaster.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.career.careermaster.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CareerRequest $request)
    {
        $career_data = $request->all();
        // dd($career_data);
        Event::fire(new CareerEvent($career_data));

        if($request->save == 'add')
        {
            return redirect()->route('career.index')->with('message','Record added successfully..')->with('message_type','success');
        }
            return back()->with('message','Record added successfully..')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $career_data = Career::find($id);
        $id = $career_data['id'];

        return view('admin.career.careermaster.edit',['career_data'=>$career_data,'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CareerRequest $request, $id)
    {
        $career_data = $request->all();
        $career_data['id'] = $id;

        Event::fire(new CareerEvent($career_data));
        if($request->save == 'Save')
        {
            return back()->with('message','Record Updated successfully')->with('message_type','success');
        }

        return redirect()->route('career.index')->with('message','Record Updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $career_id = $request->get('id');
        //dd(count($career_id));
        if(count($career_id) > 1){
            foreach ($career_id as $key => $value) {
                Career::where('id', $value)->delete();
            }
        }
        else{
            Career::where('id', $career_id)->delete();
        }    
        return back()->with('message', 'Record deleted Successfully.')
            ->with('message_type', 'success');
    }
}
