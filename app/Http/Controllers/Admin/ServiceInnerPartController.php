<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ServiceInnerpart;
use Event;
use App\Events\ServiceInnerpartEvent;
use App\Http\Requests\ServiceInnerpartRequest;

class ServiceInnerPartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $where_paramsstr = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_paramsstr .= " and ( title like \"%{$search}%\")";
            }
            $user = ServiceInnerpart::select('id','title','description','services')
            ->whereRaw($where_paramsstr, $where_params);

            $user_count = ServiceInnerpart::select('id')
            ->whereRaw($where_paramsstr, $where_params)
            ->count();

            $columns = ['id','created_at','title','description','services','updated_at'];

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $user = $user->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $user = $user->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            $user = $user->get();
            $response['iTotalDisplayRecords'] = $user_count;
            $response['iTotalRecords'] = $user_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $user;

            return $response;
        }
        return view('admin.services.service-innerpart.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.service-innerpart.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceInnerpartRequest $request)
    {
        $servicepart_data = $request->all();
        Event::fire(new ServiceInnerpartEvent($servicepart_data));

        if($request->save == 'add')
        {
            return redirect()->route('servicepart.index')->with('message','Record added successfully..')->with('message_type','success');
        }
            return back()->with('message','Record added successfully..')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicepart_data = ServiceInnerpart::find($id);
        $id = $servicepart_data['id'];

        return view('admin.services.service-innerpart.edit',['servicepart_data'=>$servicepart_data,'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceInnerpartRequest $request, $id)
    {
        $servicepart_data = $request->all();
        $servicepart_data['id'] = $id;

        Event::fire(new ServiceInnerpartEvent($servicepart_data));
        if($request->save == 'Save')
        {
            return back()->with('message','Record Updated successfully')->with('message_type','success');
        }

        return redirect()->route('servicepart.index')->with('message','Record Updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $service_inner_id = $request->id;
        if(is_array($service_inner_id))
        {
            foreach($service_inner_id as $key=>$value)
            {
                $image = ServiceInnerpart::select('inner_image')->where('id',$value)->first()->toArray();
                $i = $image['inner_image'];

                ServiceInnerpart::destroy($value);

                $path = public_path()."/images/service_inner/".$i;
                if(file_exists($path))
                {
                    unlink($path);
                }
            }    
        }
        else
        {
            $image = ServiceInnerpart::where('id',$service_inner_id)->pluck('inner_image');
            $image = $image[0];

            $path = public_path()."/images/service_inner/".$image;
            if(file_exists($path))
            {
                unlink($path);
            }
            ServiceInnerpart::destroy($service_inner_id);
        }
        return back()->with('message', 'Record deleted Successfully.')
                 ->with('message_type', 'success');
    }
}
