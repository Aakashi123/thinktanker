<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Servicemaster;
use Event;
use App\Events\ServicemasterEvent;
use App\Http\Requests\ServicemasterRequest;

class ServiceMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $where_paramsstr = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_paramsstr .= " and ( home_title like \"%{$search}%\")";
            }
            $user = Servicemaster::select('id','home_title','image')
            ->whereRaw($where_paramsstr, $where_params);

            $user_count = Servicemaster::select('id')
            ->whereRaw($where_paramsstr, $where_params)
            ->count();

            $columns = ['id','home_title','image'];

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $user = $user->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $user = $user->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            $user = $user->get();
            $response['iTotalDisplayRecords'] = $user_count;
            $response['iTotalRecords'] = $user_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $user;

            return $response;
        }
        return view('admin.services.servicemaster.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.servicemaster.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServicemasterRequest $request)
    {
        
        $servicemaster_data = $request->all();
        Event::fire(new ServicemasterEvent($servicemaster_data));

        if($request->save == 'add')
        {
            return redirect()->route('servicemaster.index')->with('message','Record added successfully..')->with('message_type','success');
        }
            return back()->with('message','Record added successfully..')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicemaster_data = Servicemaster::find($id);
        $id = $servicemaster_data['id'];

        return view('admin.services.servicemaster.edit',['servicemaster_data'=>$servicemaster_data,'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServicemasterRequest $request, $id)
    {
        $servicemaster_data = $request->all();
        $servicemaster_data['id'] = $id;

        Event::fire(new ServicemasterEvent($servicemaster_data));
        if($request->save == 'Save')
        {
            return back()->with('message','Record Updated successfully')->with('message_type','success');
        }

        return redirect()->route('servicemaster.index')->with('message','Record Updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $servicemaster_id = $request->id;
        if(is_array($servicemaster_id))
        {
            foreach($servicemaster_id as $key=>$value)
            {
                $image = Servicemaster::select('image')->where('id',$value)->first()->toArray();
                $i = $image['image'];

                Servicemaster::destroy($value);

                $path = public_path()."/images/servicemaster/".$i;
                if(file_exists($path))
                {
                    unlink($path);
                }
            }    
        }
        else
        {
            $servicemaster_id = $request->get('id');
            
            $image = Servicemaster::where('id',$servicemaster_id)->pluck('image');
            $image = $image[0];

            $path = public_path()."/images/servicemaster/".$image;
            if(file_exists($path))
            {
                unlink($path);
            }
            $user = Servicemaster::destroy($servicemaster_id);
        }
            return back()->with('message', 'Record deleted Successfully.')
                 ->with('message_type', 'success');
    }
}
