<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cms;
use Event;
use App\Events\CmsEvent;
use App\Http\Requests\CmsRequest;

class CmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $where_paramsstr = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_paramsstr .= " and ( title like \"%{$search}%\")"
                . " or link like \"%{$search}%\"";
            }
            $user = Cms::select('id','title','image','link')
            ->whereRaw($where_paramsstr, $where_params);

            $user_count = Cms::select('id')
            ->whereRaw($where_paramsstr, $where_params)
            ->count();

            $columns = ['id','title','image','link'];

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $user = $user->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $user = $user->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            $user = $user->get();
            $response['iTotalDisplayRecords'] = $user_count;
            $response['iTotalRecords'] = $user_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $user;

            return $response;
        }
        return view('admin.master.cms.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master.cms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CmsRequest $request)
    {
        $cms_data = $request->all();
        Event::fire(new CmsEvent($cms_data));

        if($request->save == 'add')
        {
            return redirect()->route('cms.index')->with('message','Record added successfully..')->with('message_type','success');
        }
            return back()->with('message','Record added successfully..')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cms_data = Cms::find($id);
        $id = $cms_data['id'];

        return view('admin.master.cms.edit',['cms_data'=>$cms_data,'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CmsRequest $request, $id)
    {
        $cms_data = $request->all();
        $cms_data['id'] = $id;

        Event::fire(new CmsEvent($cms_data));
        if($request->save == 'Save')
        {
            return back()->with('message','Record Updated successfully')->with('message_type','success');
        }

        return redirect()->route('cms.index')->with('message','Record Updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $cms_id = $request->id;
        if(is_array($cms_id))
        {
            foreach($cms_id as $key=>$value)
            {
                $image = Cms::select('image')->where('id',$value)->first()->toArray();
                $i = $image['image'];

                Cms::destroy($value);

                $path = public_path()."/images/cms/".$i;
                if(file_exists($path))
                {
                    unlink($path);
                }
            }    
        }
        else
        {
            $cms_id = $request->get('id');
            
            $image = Cms::where('id',$cms_id)->pluck('image');
            $image = $image[0];

            $path = public_path()."/images/cms/".$image;
            if(file_exists($path))
            {
                unlink($path);
            }
            $user = Cms::destroy($cms_id);
        }
            return back()->with('message', 'Record deleted Successfully.')
                 ->with('message_type', 'success');
    }
}
