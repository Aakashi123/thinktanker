<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Projectmaster;
use Event;
use App\Events\ProjectmasterEvent;
use App\Http\Requests\ProjectmasterRequest;

class ProjectMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $where_paramsstr = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_paramsstr .= " and ( title like \"%{$search}%\")";
            }
            $user = Projectmaster::select('id','title','image','link')
            ->whereRaw($where_paramsstr, $where_params);

            $user_count = Projectmaster::select('id')
            ->whereRaw($where_paramsstr, $where_params)
            ->count();

            $columns = ['id','title','image','link'];

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $user = $user->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $user = $user->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            $user = $user->get();
            $response['iTotalDisplayRecords'] = $user_count;
            $response['iTotalRecords'] = $user_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $user;

            return $response;
        }
        return view('admin.master.projectmaster.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master.projectmaster.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectmasterRequest $request)
    {
        
        $projectmaster_data = $request->all();
        Event::fire(new ProjectmasterEvent($projectmaster_data));

        if($request->save == 'add')
        {
            return redirect()->route('projectmaster.index')->with('message','Record added successfully..')->with('message_type','success');
        }
            return back()->with('message','Record added successfully..')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projectmaster_data = Projectmaster::find($id);
        $id = $projectmaster_data['id'];

        return view('admin.master.projectmaster.edit',['projectmaster_data'=>$projectmaster_data,'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectmasterRequest $request, $id)
    {
        $projectmaster_data = $request->all();
        $projectmaster_data['id'] = $id;

        Event::fire(new ProjectmasterEvent($projectmaster_data));
        if($request->save == 'Save')
        {
            return back()->with('message','Record Updated successfully')->with('message_type','success');
        }

        return redirect()->route('projectmaster.index')->with('message','Record Updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $projectmaster_id = $request->id;
        if(is_array($projectmaster_id))
        {
            foreach($projectmaster_id as $key=>$value)
            {
                $image = Projectmaster::select('image')->where('id',$value)->first()->toArray();
                $i = $image['image'];

                Projectmaster::destroy($value);

                $path = public_path()."/images/projectmaster/".$i;
                if(file_exists($path))
                {
                    unlink($path);
                }
            }    
        }
        else
        {
            $projectmaster_id = $request->get('id');
            
            $image = Projectmaster::where('id',$projectmaster_id)->pluck('image');
            $image = $image[0];

            $path = public_path()."/images/projectmaster/".$image;
            if(file_exists($path))
            {
                unlink($path);
            }
            Projectmaster::destroy($projectmaster_id);
        }
            return back()->with('message', 'Record deleted Successfully.')
                 ->with('message_type', 'success');        
    }
}
