<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Event;
use App\Model\Clientlogo;
use App\Events\ClientlogoEvent;
use App\Helpers\FileHelp;

class ClientLogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientlogo_data = Clientlogo::all();

        return view('admin.gallery.clientlogo.index',compact('clientlogo_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gallery.clientlogo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clientlogo_data = $request->all();
        Event::fire(new ClientlogoEvent($clientlogo_data));

        if($request->save == 'add')
        {
            return redirect()->route('clientlogo.index')->with('message','Record added successfully..')->with('message_type','success');
        }
            return back()->with('message','Record added successfully..')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $image = Clientlogo::select('image_name')->get()->toArray();
        // dd($image);
        foreach ($image as $key => $single_image) {

            $image_url = IMAGE_PATH . "/images/clientlogo/" . $single_image['image_name'];
            unset($image[$key]['image_name']);
            $image[$key]['name'] = $single_image['image_name'];
            $image[$key]['file'] = $image_url;
            $image[$key]['type'] = "image/png";
            $image[$key]['size'] = 87399;
        }
        $image_data = stripcslashes(json_encode($image));
        return view('admin.gallery.clientlogo.edit',compact('image_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $clientlogo_data = $request->all();
        Event::fire(new ClientlogoEvent($clientlogo_data));

        if($request->save == 'add')
        {
            return redirect()->route('clientlogo.index')->with('message','Record added successfully..')->with('message_type','success');
        }
            return back()->with('message','Record added successfully..')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $image = $request->get('image_name');
        $path = public_path()."/images/clientlogo/".$image;
        if(file_exists($path))
        { 
            unlink($path);
        }
        
        $image_id = Clientlogo::where('image_name',$image)->delete();

        return response()->json(array('success' => true), 200);
    }
}
