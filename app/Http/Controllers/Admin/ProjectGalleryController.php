<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProjectTitle;
use App\Model\ProjectGallery;
use App\Http\Requests\ProjectgalleryRequest;
use App\Helpers\FileHelp;

class ProjectGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $where_paramsstr = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_paramsstr .= " and ( title like \"%{$search}%\")";
            }
            $gallery = ProjectTitle::select('id','title')
            ->whereRaw($where_paramsstr, $where_params);

            $gallery_count = ProjectTitle::select('id')
            ->whereRaw($where_paramsstr, $where_params)
            ->count();

            $columns = ['id','title'];

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $gallery = $gallery->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $gallery = $gallery->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            $gallery = $gallery->get();
            $response['iTotalDisplayRecords'] = $gallery_count;
            $response['iTotalRecords'] = $gallery_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $gallery;

            return $response;
        }
        return view('admin.gallery.projectgallery.index');
    }

    public function create()
    {
        return view('admin.gallery.projectgallery.create');
    }

    public function store(ProjectgalleryRequest $request)
    {
        $title = $request->title;
        $projectgallery_data = ProjectTitle::create(['title'=>$title]);

        $image = $request->image;
        foreach($image as $key=>$value)
        {
            $file = $image[$key];
            $filename = FileHelp::getfilename($file);
            $path = UPLOAD_PATH."/images/projectgallery";
            if(!file_exists($path))
            {
                mkdir($path,0777);
            }
            $file->move($path,$filename);

            $image_data = new ProjectGallery();

            $image_data->image = $filename;
            $image_data->title_id = $projectgallery_data['id'];
            $image_data->save();
        }

        if($request->save_new == 'Save & New')
        {
            return back()->with('message','Record added Successfully')->with('message_type','success');
        }
        return redirect()->route('projectgallery.index')->with('message','Record added Successfully')->with('message_type','success');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $projectgallery_data = ProjectTitle::find($id);
        $id = $projectgallery_data['id'];
        $gallary_images = ProjectGallery::select('image')->where('title_id', $id)->get()->toArray();

        foreach ($gallary_images as $key => $single_image) {

            $image_url = IMAGE_PATH . "/images/projectgallery/" . $single_image['image'];
            $image[$key]['name'] = $single_image['image'];
            $image[$key]['type'] = "image/png";
            $image[$key]['size'] = 87399;
            $image[$key]['file'] = $image_url;
            $image[$key]['data']['url'] = $image_url;
        }
        if(!empty($image))
        {
            $image_data = stripcslashes(json_encode($image));
        }

        return view('admin.gallery.projectgallery.edit',compact('projectgallery_data','id','image_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectgalleryRequest $request, $id)
    {
        $projectgallery_data = $request->all();
        $projectgallery_data['id'] = $id;
        if(isset($projectgallery_data['id']))
        {
            $id = $projectgallery_data['id'];
        }

        $save = ProjectTitle::firstorNew(['id'=>$id]);
        $save->fill($projectgallery_data);

        if(!empty($projectgallery_data['image']))
        {
            foreach($projectgallery_data['image'] as $key=>$value)
            {
                $file = $projectgallery_data['image'][$key];
                $filename = FileHelp::getfilename($file);

                $path = UPLOAD_PATH."/images/projectgallery/";
                $file->move($path,$filename);

                $save_image = new ProjectGallery();
                $save_image->image = $filename;
                $save_image->title_id = $id;
                $save_image->save();
            } 
        }
        $save->save();
        if($request->save == 'Save')
        {
            return back()->with('message','Record Updated successfully')->with('message_type','success');
        }
        
        return redirect()->route('projectgallery.index')->with('message','Record Updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(!empty($request->id))
        {
            $id = $request->id;
            if(is_array($id))
            {
                foreach($id as $key=>$value)
                {
                    $image = ProjectGallery::where('title_id',$value)->pluck('image');
                    foreach($image as $key=>$v)
                    {
                        $i = $image[$key];

                        $path = UPLOAD_PATH."/images/projectgallery/".$i;
                        if(file_exists($path))
                        {
                            unlink($path);
                        }
                    }

                    $multi_id = ProjectGallery::where('title_id',$id)->delete();

                    ProjectTitle::destroy($id);
                }
            }
            else
            {
                $image = ProjectGallery::where('title_id',$id)->pluck('image');
                    foreach($image as $key=>$v)
                    {
                        $i = $image[$key];

                        $path = UPLOAD_PATH."/images/projectgallery/".$i;
                        if(file_exists($path))
                        {
                            unlink($path);
                        }
                    }

                    $multi_id = ProjectGallery::where('title_id',$id)->delete();

                    ProjectTitle::destroy($id);
            }

            return back()->with('message','Record Deleted Successfully')->with('message_type','success');
        }
    }

    public function deleteImage(Request $request)
    {
        $image_data = $request->all();

        $image = $image_data['image_name'];

        $image_unlink_path = public_path()."/images/projectgallery/".$image;
        
        if(file_exists($image_unlink_path))
        {
            $unlink_old_image = unlink($image_unlink_path);
        }

        $image_id = ProjectGallery::where('image', $image)->delete();

        return response()->json(array('success' => true), 200);
    }
}
