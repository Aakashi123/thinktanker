<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Testimonial;
use Event;
use App\Http\Requests\TestimonialRequest;
use App\Events\TestimonialEvent;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $where_paramsstr = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_paramsstr .= " and ( name like \"%{$search}%\")";
            }
            $user = Testimonial::select('id', 'name','image')
            ->whereRaw($where_paramsstr, $where_params);

            $user_count = Testimonial::select('id')
            ->whereRaw($where_paramsstr, $where_params)
            ->count();

            $columns = ['id','name','image'];

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $user = $user->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $user = $user->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            $user = $user->get();
            $response['iTotalDisplayRecords'] = $user_count;
            $response['iTotalRecords'] = $user_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $user;

            return $response;
        }
        return view('admin.home.testimonial.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.home.testimonial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TestimonialRequest $request)
    {
        $testimonial_data = $request->all();
        Event::fire(new TestimonialEvent($testimonial_data));

        if($request->save == 'add')
        {
            return redirect()->route('testimonial.index')->with('message','Record added successfully..')->with('message_type','success');
        }
            return back()->with('message','Record added successfully..')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $testimonial_data = Testimonial::find($id);
        $id = $testimonial_data['id'];

        return view('admin.home.testimonial.edit',['testimonial_data'=>$testimonial_data,'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TestimonialRequest $request, $id)
    {
         $testimonial_data = $request->all();
        $testimonial_data['id'] = $id;

        Event::fire(new TestimonialEvent($testimonial_data));
        if($request->save == 'Save')
        {
            return back()->with('message','Record Updated successfully')->with('message_type','success');
        }

        return redirect()->route('testimonial.index')->with('message','Record Updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $testimonial_id = $request->id;
        if(is_array($testimonial_id))
        {
            foreach($testimonial_id as $key=>$value)
            {
                $image = Testimonial::select('image')->where('id',$value)->first()->toArray();
                $i = $image['image'];

                Testimonial::destroy($value);

                $path = public_path()."/images/testimonial/".$i;
                if(file_exists($path))
                {
                    unlink($path);
                }
            }    
        }
        else
        {
            $testimonial_id = $request->get('id');
        
            $image = Testimonial::where('id',$testimonial_id)->pluck('image');
            $image = $image[0];
            $path = public_path()."/images/testimonial/".$image;
            if(file_exists($path))
            {
                unlink($path);
            }
            $user = Testimonial::destroy($testimonial_id);
        }
        return back()->with('message', 'Record deleted Successfully.')
              ->with('message_type', 'success');
    }
}
