<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Slider;
use Event;
use App\Http\Requests\SliderRequest;
use App\Events\SliderEvent;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider_data = Slider::get();
        return view('admin.home.slider.index',compact('slider_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.home.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderRequest $request)
    {

        $slider_data = $request->all();
        // dd($slider_data);
        Event::fire(new SliderEvent($slider_data));

        if($request->save == 'add')
        {
            return redirect()->route('slider.index')->with('message','Record added successfully..')->with('message_type','success');
        }
            return back()->with('message','Record added successfully..')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider_data = Slider::find($id);
        return view('admin.home.slider.edit',compact('slider_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SliderRequest $request, $id)
    {
        //dd($request->all());
        $slider_data = $request->all();
        $slider_data['id'] = $id;
        Event::fire(new SliderEvent($slider_data));
        if($request->save == 'Save')
        {
            return back()->with('message','Record Updated successfully')->with('message_type','success');
        }

        return redirect()->route('slider.index')->with('message','Record Updated Successfully')->with('message_type','success');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $slider_id = $request->get('id');
        $image = Slider::where('id',$slider_id)->pluck('image');
        $image = $image[0];
        $path = public_path()."/images/slider/".$image;
        if(file_exists($path))
        {
            unlink($path);
        }
        $user = Slider::destroy($slider_id);
        return back()->with('message', 'Record deleted Successfully.')
            ->with('message_type', 'success');
    }

    public function order(Request $request){
        $order = $request->get('order');
        foreach ($order as $key => $value) 
        {
            $index = $key + 1;
            Slider::where('id', $value)->update(array('order' => $index));
        }
    }
}
