<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Educational;

class EducationalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $where_paramsstr = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_paramsstr .= " and ( educational_name like \"%{$search}%\")";
            }
            $education = Educational::select('id', 'educational_name')
            ->whereRaw($where_paramsstr, $where_params);

            $education_count = Educational::select('id')
            ->whereRaw($where_paramsstr, $where_params)
            ->count();

            $columns = ['id','educational_name'];

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $education = $education->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $education = $education->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            $education = $education->get();
            $response['iTotalDisplayRecords'] = $education_count;
            $response['iTotalRecords'] = $education_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $education;

            return $response;
        }
        return view('admin.career.educationalmaster.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.career.educationalmaster.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $education_data= new Educational();

        $education_data->educational_name= $request['educational_name'];

        $education_data->save();

        return redirect()->route('educational.index')->with('message','Record added successfully..')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $education_data = Educational::find($id);
        $id = $education_data['id'];

        return view('admin.career.educationalmaster.edit',['education_data'=>$education_data,'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request_data = $request->all();
       
        $educationa_data = Educational::find($id);
    
        $data =Educational::where('id', $id)->update([
            'educational_name' => $request_data['educational_name']]);

        return redirect()->route('educational.index')->with('message', 'Record updated Successfully.')
            ->with('message_type', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $education_id = $request->get('id');
        
        if(is_array($education_id)){
            foreach ($education_id as $key => $value) {
                Educational::where('id', $value)->delete();
            }
        }
        else{
            Educational::where('id', $education_id)->delete();
        }    
        return back()->with('message', 'Record deleted Successfully.')
            ->with('message_type', 'success');
    }
}
