<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Technology;

class TechnologyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $where_paramsstr = '1 = ?';
            $where_params = [1];

            if ($request->has('sSearch')) {
                $search = $request->get('sSearch');
                $where_paramsstr .= " and ( technology_name like \"%{$search}%\")";
            }
            $technology = Technology::select('id', 'technology_name')
            ->whereRaw($where_paramsstr, $where_params);

            $technology_count = Technology::select('id')
            ->whereRaw($where_paramsstr, $where_params)
            ->count();

            $columns = ['id','technology_name'];

            if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                $technology = $technology->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
            }

            if ($request->has('iSortCol_0')) {
                for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                    $column = $columns[$request->get('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $technology = $technology->orderBy($column, $request->get('sSortDir_' . $i));
                }
            }
            $technology = $technology->get();
            $response['iTotalDisplayRecords'] = $technology_count;
            $response['iTotalRecords'] = $technology_count;

            $response['sEcho'] = intval($request->get('sEcho'));

            $response['aaData'] = $technology;

            return $response;
        }
        return view('admin.career.technologymaster.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.career.technologymaster.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $technology_data= new Technology();

        $technology_data->technology_name= $request['technology_name'];

        $technology_data->save();

        return redirect()->route('technology.index')->with('message','Record added successfully..')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $technology_data = Technology::find($id);
        $id = $technology_data['id'];

        return view('admin.career.technologymaster.edit',['technology_data'=>$technology_data,'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request_data = $request->all();
       
        $technology_data = Technology::find($id);
    
        $data =Technology::where('id', $id)->update([
            'technology_name' => $request_data['technology_name']]);

        return redirect()->route('technology.index')->with('message', 'Record updated Successfully.')
            ->with('message_type', 'success');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $technology_id = $request->get('id');
        
        if(is_array($technology_id)){
            foreach ($technology_id as $key => $value) {
                Technology::where('id', $value)->delete();
            }
        }
        else{
            Technology::where('id', $technology_id)->delete();
        }    
        return back()->with('message', 'Record deleted Successfully.')
            ->with('message_type', 'success');
    }
}
