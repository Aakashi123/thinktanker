<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Frontend\ContactUs;
use App\Http\Requests\Frontend\ContactUsRequest;


class ContactUsController extends Controller
{
	public function index(Request $request)
	{
         
        
        $contactus_data = ContactUs::create($request->all());
		return view('frontend.contact.index');
	}
    public function SaveContact(ContactUsRequest $request)
    {

        $about_enquiry = implode(',', (array) $request->get('about_enquiry'));
        $contactus_data= new ContactUs();
        $contactus_data->fname= strip_tags($request['fname']);
        $contactus_data->email= strip_tags($request['email']);
        $contactus_data->contact= $request['contact'];
        $contactus_data->about_enquiry = $about_enquiry;
        $contactus_data->message= strip_tags($request['message']);
     
        $contactus_data->save();

    	return back()->with('message','Record added successfully..')->with('message_type','success'); 
    }
}
