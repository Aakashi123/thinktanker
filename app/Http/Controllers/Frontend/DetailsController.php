<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ServiceInnerpart;
use App\Model\Servicemaster;
use App\Model\Cms;
use DB;
use App\Model\Technology;
use App\Model\Educational;
use App\Model\Career;

class DetailsController extends Controller
{
     public function details(Request $request)
    {
    	$slug = $request->segment(1);
        if($slug == 'homepage')
        {
            return redirect()->route('homepage.index');
        }

        else if($slug == 'about-us')
        {
            $about_us_data = Cms::select('title','description','metatitle','metadescription','metakeyword')->where('slug',$slug)->first();
        	return view('frontend.about.index',compact('about_us_data'));
        }

        else if($slug == 'contact-us')
        {
            return view('frontend.contact.index');
        }

        else if($slug == 'career')
        {
            $technology_list = Technology::orderBy('technology_name','asc')->pluck('technology_name','id')->toArray();

            $educational_list = Educational::orderBy('educational_name','asc')->pluck('educational_name','id')->toArray();

            $career_meta_data = Career::select('metatitle','metadescription','metakeyword')->first();
    
            return view('frontend.career.index',compact('technology_list','educational_list','career_meta_data'));
        }

        else
        {
            echo "Hi";
        }
    }

    public function getservices(Request $request){
        $slug = $request->segment(2);
        
        if(in_array($slug,config('services-menu.services_menu_url')))
        {
            $service_part_data = ServiceInnerpart::select('title','description','inner_image')->where('services','like','%'.$slug.'%')->get();
            // dd($service_part_data);
            $service_upper_data = Servicemaster::select('inner_title','inner_description','metatitle','metadescription','metakeyword')->where('slug','like','%'.$slug.'%')->first();
            // dd($service_upper_data);
            return view('frontend.services.website_design',compact('service_part_data','service_upper_data'));
        }
        else{
            echo "hi";
        }
    }
}
