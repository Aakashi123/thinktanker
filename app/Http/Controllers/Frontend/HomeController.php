<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Servicemaster;

class HomeController extends Controller
{
    public function index()
    {
    	$homepage_service_data = Servicemaster::select('home_title','home_description','inner_title','inner_description','slug')->
    	whereNotIn('slug',['digital-marketing','mobile-application'])->get();

    	return view('frontend.homepage.index',compact('homepage_service_data'));
    }
}
