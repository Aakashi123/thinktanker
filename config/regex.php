<?php
	return[

		'testimonial' => [

			'name' => '/^[a-zA-Z ]+$/'
		],
		'slider' => [
			'title' => '/^[a-zA-Z-\'" ]+$/',
		],
		'generalinfo' => [
			'title' => '/^[a-zA-Z ]+$/',
			'subtitle' => '/^[a-zA-Z ]+$/',
			'mobile1' => '/^[0-9]+$/',
			'mobile2' => '/^[0-9]+$/',
		],
		'project' => [
			'title' => '/^[a-zA-Z0-9-.\'" ]+$/',
		],
		//for all slug
		'slug' => [
			'slug' => '/^[a-zA-Z0-9-]+$/'
		],
		'cms' => [
			'title' => '/^[a-zA-Z-.\'" ]+$/',
		],
		'career' => [
			'title' => '/^[a-zA-Z0-9-.\'" ]+$/',
		],
	];
?>