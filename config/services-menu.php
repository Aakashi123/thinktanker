<?php

	return[
		'services_menu_url' => [
			'website-design',
			'web-development',
			'ecommerce-website',
			'web-application',
			'mobile-application',
			'android-application',
			'ios-application-development',
			'digital-marketing',
			'website-maintenance',
		]
	];