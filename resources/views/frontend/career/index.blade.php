@extends('frontend.layout.index')
@section('meta')
    <meta name="title" content="{{$career_meta_data['metatitle']}}">
    <meta name="description" content="{{$career_meta_data['metadescription']}}">
    <meta name="keywords" content="{{$career_meta_data['metakeyword']}}">
@stop
@section('dubline')
<meta name="dc.source" CONTENT="<?= config('app.url')."/".Request::path()?>">
    <meta name="dc.title" CONTENT="{{$career_meta_data['metatitle']}}">
    <meta name="dc.description" CONTENT="{{$career_meta_data['metadescription']}}">
    <meta name="dc.format" CONTENT="text/html">
    <meta name="dc.language" CONTENT="en">
@stop
@section('content')
	<section class="banner-section contact-section">
      	<div class="overlay">
        	<div class="container">
          		<div class="type-wrap">
            		<p class="home-banner-title">Get in Touch</p>
            		<p class="home-banner-text">Have a little something, something you wanna talk to us about? Well give us a ring, send us an email, or fill out that nifty form below.</p>
          		</div>
        	</div>
      	</div>
	</section>
	<section class="contact-block">
    	<div class="container">
        	<div class="row">
				<div class="col-sm-12">
		            <h2 class="maintitle text-center">Apply</h2>
		            <div class="row">
		              	<div class="col-md-6">
		                	<?= Form::open(['method'=>'post', 'route' => 'frontend-career.store','files'=>true]) ?>
		                	
		                	  	<div class="form-group">
		                    	<label class="col-form-label">Please enter your full name</label>
		                    	<input class="form-control" type="text" name="fname" placeholder="Full Name" maxlength="50">
		                    	<div class="help-block">
		                        	{{$errors->first('fname')}}
		                    	</div>
		                  	</div>
		                  	<div class="form-group">
		                    	<label class="col-form-label">Please enter your birth date</label>
		                    	<input class="form-control" type="date" name="birthdate">
		                    	<div class="help-block">
		                        	{{$errors->first('birthdate')}}
		                    	</div>
		                  	</div>
		                  	<div class="form-group">
		                    	<label class="col-form-label">Please select your educational background</label>
		                    	<?= Form::select('educational',$educational_list,old('educational'),['class'=>'form-control','placeholder'=>'---Select---']) ?>
		                    	<div class="help-block">
		                        	{{$errors->first('educational')}}
		                    	</div>
		                  	</div>
		                  	<div class="form-group">
		                    	<label class="col-form-label">Please enter your address</label>
		                    	<textarea class="form-control" name="address" placeholder="Address" rows="3"></textarea>
		                    	<div class="help-block">
		                        	{{$errors->first('address')}}
		                    	</div>
		                  	</div>
		                </div>
		                <div class="col-md-6">
		                  	<div class="form-group">
		                    	<label class="col-form-label">Position Applying for</label>
		                    	<?= Form::select('position',$technology_list,old('position'),['class'=>'form-control','placeholder'=>'---Select---']) ?>
		                    	<div class="help-block">
		                        	{{$errors->first('position')}}
		                    	</div>
		                  	</div>
		                  	<div class="form-group">
		                    	<label class="col-form-label">Please enter your email address</label>
		                    	<input type="email" name="email" class="form-control" placeholder="Email address">
		                    	<div class="help-block">
		                        	{{$errors->first('email')}}
		                    	</div>
		                  	</div>
		                  	<div class="form-group">
			                    <label class="col-form-label">Please enter your contact number</label>
			                    <input class="form-control number_only" name="contact" type="text" placeholder="Contact number" maxlength="10">
			                    <div class="help-block">
			                        {{$errors->first('contact')}}
			                    </div>
		                  	</div>
		                  	<div class="form-group">
		                    	<label class="col-form-label">Upload Your Resume</label>
		                    	<input type="file" name="resume_name">
		                    	<div class="help-block">
		                        	{{$errors->first('resume_name')}}
		                    	</div>
		                  	</div>
		                  	<button class="btn-blackmain btn-block" name="save" value="add" type="submit">Submit</button>
		                	<?= Form::close() ?>
	            		</div>
	        		</div>
	    		</div>
			</div>
		</div>
	</section>
@stop

@section('js')
<script type="text/javascript">
    $(".number_only").keypress(function(h){if(8!=h.which&&0!=h.which&&(h.which<48||h.which>57))return!1});
</script>
	@include('admin.layout.alert')
@stop