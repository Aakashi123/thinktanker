@extends('frontend.layout.index')
 @section('meta')
    <meta name="title" content="{{$service_upper_data['metatitle']}}">
    <meta name="description" content="{{$service_upper_data['metadescription']}}">
    <meta name="keywords" content="{{$service_upper_data['metakeyword']}}">
@stop
@section('dubline')
<meta name="dc.source" CONTENT="<?= config('app.url')."/".Request::path()?>">
    <meta name="dc.title" CONTENT="{{$service_upper_data['metatitle']}}">
    <meta name="dc.description" CONTENT="{{$service_upper_data['metadescription']}}">
    <meta name="dc.format" CONTENT="text/html">
    <meta name="dc.language" CONTENT="en">
@stop
@section('content')
<section class="banner-section webdesign-section">
    <div class="overlay">
        <div class="container">
            <div class="type-wrap">
                <p class="home-banner-title">WEBSITE DESIGN</p>
                <p class="home-banner-text">We strive for excellence in web designing, development and digital marketing. We spend our time brightening up the world wide web.</p>
                <div class="align-button">
                    <div class="button-wrap"><a class="btn" href="javascript:;">GET STARTED NOW</a></div>
                    </div>
            </div>
        </div>
    </div>
</section>
<section class="webdesign-services">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="maintitle text-center">{{$service_upper_data['inner_title']}}</h2>
                {{$service_upper_data['inner_description']}}
                <div class="row">
                    <div class="col-md-4 text-center">
                        <div class="wd-con">
                            <div class="hexagon"><img class="border-hexa" alt="" src=<?= IMAGE_PATH."/frontend/images/website-design/hexa-border-icon1.png" ?>><img class="solid-hexa" alt="" src=<?= IMAGE_PATH."/frontend/images/website-design/hexa-solid-icon1.png" ?>></div>
                            <p><span>Brand</span><br>building through website.</p>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="wd-con">
                            <div class="hexagon"><img class="border-hexa" alt="" src=<?= IMAGE_PATH."/frontend/images/website-design/hexa-border-icon2.png" ?>><img class="solid-hexa" alt="" src=<?= IMAGE_PATH."/frontend/images/website-design/hexa-solid-icon2.png" ?>></div>
                            <p><span>mobile</span><br>and tablet ready web design..</p>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="wd-con">
                            <div class="hexagon"><img class="border-hexa" alt="" src=<?= IMAGE_PATH."/frontend/images/website-design/hexa-border-icon3.png" ?>><img class="solid-hexa" alt="" src=<?= IMAGE_PATH."/frontend/images/website-design/hexa-solid-icon3.png" ?>></div>
                            <p><span>user</span><br>friendly web designs..</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="webservice-contact text-center section-gray">
    <div class="container">
        <h2>We Love To Make Beautiful Web Designs</h2>
        <p>Website Design Company in Surat, India</p><a class="btn-main" href="<?=url('/contact-us')?>">contact us now!</a>
    </div>
</section>
<section class="websevice-block">
    <div class="container">
        <div class="row">
            @foreach($service_part_data as $data)
            <div class="col-md-4">
                <div class="we-con1">
                    <div class="text-center"><img alt="" src=<?= IMAGE_PATH."/images/service_inner/".$data['inner_image'] ?>>
                    <h5>{{$data['title']}}</h5>
                    </div>
                    <p><?= $data['description']?></p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@stop