<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <!--favicon icon-->
    <link href=<?= IMAGE_PATH."/frontend/images/favicon.png" ?> rel="icon">
    <!-- title-->
    <title>Thinktanker</title>
    @include('frontend.layout.css')
    @yield('css')
    @yield('meta')
    @yield('dubline')
  </head>
  <body>
    <!--header section start here-->
    @include('frontend.layout.header')
    <div class="clearfix"></div>
    @yield('content')
    <!--headersection end here-->
    @include('frontend.layout.footer')
    @include('frontend.layout.js')
    @yield('js')
  </body>
</html>