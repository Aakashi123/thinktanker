<header>
    <div class="header-section-top text-right">
        <div class="container">
            <div class="top-bar-item"><a><i class="far fa-clock"></i>{{$general_data['time']}}</a></div>
            <div class="top-bar-item top-bar-item-email">
                <a href="mailto:hello@thinktanker.in">
                    <i class="fas fa-envelope"></i>{{$general_data['email']}}
                </a>
            </div>
            <div class="top-bar-item top-bar-item-phone"><a href="tel:9033155300"><i class="fas fa-mobile-alt"></i>+91 {{$general_data['mobile1']}}</a></div>
        </div>
    </div>
    <div class="header-section-bottom">
        <div class="headersection">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-12">
                        <button class="menuicon" type="button"><span class="menuline"></span><span class="menuline"></span><span class="menuline"></span></button><a class="logo align-self-center" href="{{ route('homepage.index')}}">{{$general_data['title']}}</a>
                    </div>
                    <div class="col-lg-8 col-12 menusection">
                        <ul class="nav float-right" id="menusection">
                        <li class="closemenu"><span class="far fa-times-circle"></span></li>
                        @foreach($menuitems as $m)
                            @if($m->children->count()>0)
                                @if(isset($set_active_menu_items))
                                    @if(in_array($m->id, array_keys($set_active_menu_items)))
                                        <li class="nav-item active">
                                    @else
                                        <li class="nav-item">
                                    @endif
                                @else
                                    <li class="nav-item">
                                @endif
                                    <a class="nav-link nav-link-submenu" href="javascript:;">{{$m->label}} <span class="navbar-nav-arrow"><i class="fas fa-angle-down"></i></span></a>
                                    <ul class="nav nav-submenu flex-column">
                                        @foreach($m->children as $sub)
                                            @if($sub->subchildren->count()>0)
                                                @if(isset($set_active_menu_items))
                                                    @if(in_array($sub->id, array_keys($set_active_menu_items)))
                                                        <li class="nav-item text-left active">
                                                    @else
                                                        <li class="nav-item text-left">
                                                    @endif
                                                @else
                                                    <li class="nav-item text-left">
                                                @endif
                                                <a class="nav-link nav-link-subsubmenu" href="javascript:;">{{$sub->label}}</a>
                                                    <ul class="nav nav-sub-submenu">
                                                        @foreach($sub->subchildren as $da)
                                                            @if(isset($set_active_menu_items))
                                                                @if(in_array($da->id, array_keys($set_active_menu_items)))
                                                                    <li class="nav-item text-left active">
                                                                @else
                                                                    <li class="nav-item text-left">
                                                                @endif
                                                            @else
                                                                <li class="nav-item text-left">
                                                            @endif
                                                            <a class="nav-link" href="<?= route('details.get',$da->link)?>">{{ $da->label }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @else
                                            @if(isset($set_active_menu_items))
                                                @if(in_array($sub->id, array_keys($set_active_menu_items)))
                                                    <li class="nav-item text-left active">
                                                @else
                                                    <li class="nav-item text-left">
                                                @endif
                                            @else
                                                <li class="nav-item text-left">
                                            @endif
                                            <a class="nav-link" href="<?= route('service.get',$sub->link)?>">{{$sub->label}}</a></li>

                                            @endif
                                        @endforeach
                                        </ul>
                                    </li>
                            @else
                                @if(isset($set_active_menu_items))
                                    @if(in_array($m->id, array_keys($set_active_menu_items)))
                                        <li class="nav-item active">
                                    @else
                                        <li class="nav-item">
                                    @endif
                                @else
                                    <li class="nav-item">
                                @endif
                                    <a class="nav-link" href="<?= route('details.get',$m->link)?>">{{$m->label}}</a></li>
                            @endif
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>