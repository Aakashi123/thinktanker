<footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <aside class="col-md-8">
                    <div class="footer-row">
                        <div class="footer-title"><a href="javascript:;">Services</a></div>
                        <div class="footer-ulsection">
                            <div class="footer-box">
                                <div class="row">
                                    <div class="footer-services col-md-6 col-sm-6"><a href="<?= route('service.get','website-design')?>"><i class="fas fa-desktop mr-10"></i>website design</a></div>
                                    <div class="footer-services col-md-6 col-sm-6"><a href="<?= route('service.get','website-development')?>"><i class="fas fa-file-code mr-10"></i>web development</a></div>
                                    <div class="footer-services col-md-6 col-sm-6"><a href="<?= route('service.get','e-commerce-websites')?>"><i class="fas fa-shopping-cart mr-10"></i>e-commerce websites</a></div>
                                    <div class="footer-services col-md-6 col-sm-6"><a href="<?= route('service.get','web-application')?>"><i class="fab fa-android mr-10"></i>Web Applications</a></div>
                                    <div class="footer-services col-md-6 col-sm-6"><a href="<?= route('service.get','mobile-application')?>"><i class="fas fa-mobile-alt mr-10"></i>Mobile Application Development</a></div>
                                    <div class="footer-services col-md-6 col-sm-6"><a href="<?= route('service.get','digital-marketing')?>"><i class="fas fa-chart-bar mr-10"></i>online (Digital) marketing</a></div>
                                    <div class="footer-services col-md-6 col-sm-6"><a href="<?= route('service.get','website-maintenance')?>"><i class="fab fa-searchengin mr-10"></i>Website Maintenance</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
                <aside class="col-md-4">
                    <div class="footer-row">
                        <div class="footer-title"><a href="<?= route('homepage.index')?>">Home</a></div>
                        <div class="footer-title"><a href="<?= route('details.get','about-us')?>">About Us</a></div>
                        <div class="footer-title"><a href="javascript:;">Portfolio</a></div>
                        <div class="footer-title"><a href="javascript:;">Career</a></div>
                        <div class="footer-title"><a href="javascript:;">Blog</a></div>
                        <div class="footer-title"><a href="<?= route('details.get','contact-us')?>">Contact Us</a></div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="footer-sales">
                        <sapn class="footer-phone"><a class="Contact-list" href="tel:+91 903 315 5300">+91 {{$general_data['mobile1']}}</a></sapn><span class="footer-delimiter hidden-xs">| </span>
                        <sapn class="footer-phone"><a class="Contact-list" href="tel:+91 903 315 5300">+91 {{$general_data['mobile2']}}</a></sapn><span class="footer-delimiter hidden-xs">| </span><span class="footer-email"><a class="contact-list" href="mailto:hello@thinktanker.in">{{$general_data['email']}}</a></span>
                    </div>
                    <div class="footer-address">
                        <p><?= strip_tags($general_data['address'])?></p>
                    </div>
                    <div class="footer-copyright">
                        <p>Copyright © <?php echo date('Y') ?> {{$general_data['copyright']}} </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <ul class="justify-content-center footer-soc pl-0">
                        @if(!empty($general_data['facebook']))
                            <li><a href="<?= $general_data['facebook']?>"><i class="fab fa-facebook"></i></a></li>
                        @endif
                        @if(!empty($general_data['twitter']))
                            <li><a href="<?= $general_data['twitter']?>"><i class="fab fa-twitter"></i></a></li>
                        @endif
                        @if(!empty($general_data['instagram']))
                            <li><a href="<?= $general_data['instagram']?>"><i class="fab fa-instagram"></i></a></li>
                        @endif
                        @if(!empty($general_data['linkedin']))
                            <li><a href="<?= $general_data['linkedin']?>"><i class="fab fa-linkedin"></i></a></li>
                        @endif
                        @if(!empty($general_data['google']))
                            <li><a href="<?= $general_data['google']?>"><i class="fab fa-google-plus"></i></a></li>
                        @endif
                        @if(!empty($general_data['youtube']))
                            <li><a href="<?= $general_data['youtube']?>"><i class="fab fa-youtube"></i></a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>