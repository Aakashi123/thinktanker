@extends('frontend.layout.index')
@section('meta')
    <meta name="title" content="{{$about_us_data['metatitle']}}">
    <meta name="description" content="{{$about_us_data['metadescription']}}">
    <meta name="keywords" content="{{$about_us_data['metakeyword']}}">
@stop
@section('dubline')
<meta name="dc.source" CONTENT="<?= config('app.url')."/".Request::path()?>">
    <meta name="dc.title" CONTENT="{{$about_us_data['metatitle']}}">
    <meta name="dc.description" CONTENT="{{$about_us_data['metadescription']}}">
    <meta name="dc.format" CONTENT="text/html">
    <meta name="dc.language" CONTENT="en">
@stop
@section('content')
<section class="banner-section about-section">
    <div class="overlay">
        <div class="container">
            <div class="type-wrap">
                <p class="home-banner-title">Company overview</p>
                <p class="home-banner-text">THINK TANKER has been delivering Website Development, Customized Web Application,Mmobile Application (Android, iOS) Development and related IT services. We combine proven methodologies, business domain knowledge and technology expertise of skilled software professionals to deliver high quality solutions.</p>
            </div>
        </div>
    </div>
</section>
<section class="about-block">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="maintitle text-center">{{$about_us_data['title']}}</h2>
                <p><?=$about_us_data['description']?></p>
            </div>
        </div>
    </div>
</section>
@stop