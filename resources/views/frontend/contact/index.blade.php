@extends('frontend.layout.index')
@section('content')
<section class="banner-section contact-section">
    <div class="overlay">
        <div class="container">
            <div class="type-wrap">
                <p class="home-banner-title">Get in Touch</p>
                <p class="home-banner-text">Have a little something, something you wanna talk to us about? Well give us a ring, send us an email, or fill out that nifty form below.</p>
            </div>
        </div>
    </div>
</section>
<section class="contact-block">
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-md-11">
                    <h2 class="maintitle text-center">Directions to find us</h2>
                    <p>Just type your postcode or location in to the box below and the next screen will provide you directions to our office.</p>
                    <form action="https://maps.google.co.in/maps" method="get" target="_blank">
                        <div class="form-group">
                            <input class="form-control directions" type="text" name="saddr" placeholder="Enter your location or postcode">
                            <input type="hidden" name="daddr" value="B-301, Girivar Shopping Complex, Opp. Reliance Petrol Pump, S.P. Ring Road, Vastral, Ahmedabad, Gujarat 382418">
                        </div>
                        <button class="btn-blackmain btn-block" type="submit">Get directions</button>
                    </form>
                    <div class="row">
                        <div class="col-md-6 contact-detail mt-40">
                            <h5 class="mb-10">Hours</h5>
                            <p>{{$general_data['time']}}<br>Sun - Closed</p>
                        </div>
                        <div class="col-md-6 contact-detail mt-40">
                            <h5 class="mb-10">Contact</h5>
                            <p><a href="tel:9033155300"><i class="fas fa-mobile-alt mr-5"></i>+91 {{$general_data['mobile1']}}</a><br><a href="mailto:hello@thinktanker.in"><i class="fas fa-envelope mr-5"></i>{{$general_data['email']}}</a></p>
                            <ul class="list-unstyled justify-content-center footer-soc pl-0">
                                @if(!empty($general_data['facebook']))
                                    <li class="d-inline mr-10><a href="<?= $general_data['facebook']?>"><i class="fab fa-facebook"></i></a></li>
                                @endif
                                @if(!empty($general_data['twitter']))
                                    <li class="d-inline mr-10><a href="<?= $general_data['twitter']?>"><i class="fab fa-twitter"></i></a></li>
                                @endif
                                @if(!empty($general_data['instagram']))
                                    <li class="d-inline mr-10><a href="<?= $general_data['instagram']?>"><i class="fab fa-instagram"></i></a></li>
                                @endif
                                @if(!empty($general_data['linkedin']))
                                    <li class="d-inline mr-10><a href="<?= $general_data['linkedin']?>"><i class="fab fa-linkedin"></i></a></li>
                                @endif
                                @if(!empty($general_data['google']))
                                    <li class="d-inline mr-10><a href="<?= $general_data['google']?>"><i class="fab fa-google-plus"></i></a></li>
                                @endif
                                @if(!empty($general_data['youtube']))
                                    <li class="d-inline mr-10><a href="<?= $general_data['youtube']?>"><i class="fab fa-youtube"></i></a></li>
                                @endif
                            </ul>
                        </div>
                        <div class="col-md-6 contact-detail mt-40">
                            <h5 class="mb-10">Ahmedabad Address</h5>
                            <p class="mb-5">{{$general_data['address']}}</p>
                        </div>
                        <div class="col-md-6 contact-detail mt-40">
                            <h5 class="mb-10"> Mumbai Address</h5>
                            <p class="mb-5">{{$general_data['address1']}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-md-11">
                    <h2 class="maintitle text-center">Ask a Question</h2>
                    <?= Form::open(['method'=>'post', 'route' => 'contact-us.store']) ?>

                    <div class="form-group">
                        <label class="col-form-label">Please enter your full name</label>
                        <input class="form-control" type="text" name="fname" placeholder="Name">
                        <div class="help-block">
                            {{$errors->first('fname')}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Please enter your email address</label>
                        <input type="email" name="email" class="form-control" placeholder="Email address">
                        <div class="help-block">
                            {{$errors->first('email')}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Please enter your contact number</label>
                        <input class="form-control number_only" name="contact" type="text" placeholder="Contact number" maxlength="10">
                        <div class="help-block">
                            {{$errors->first('contact')}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">What is the enquiry about</label>
                        <div class="row">
                            <div class="col-md-4">
                                <p>Web Design</p>
                                <label class="switch">
                                    <input type="checkbox" name="about_enquiry[]" value="webdesign"><span class="slider round"></span>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <p>Web Devlopment</p>
                                <label class="switch">
                                    <input type="checkbox" name="about_enquiry[]" value="webdevelopment"><span class="slider round"></span>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <p>Mobile Application</p>
                                <label class="switch">
                                    <input type="checkbox" name="about_enquiry[]" value="mobileapplication"><span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Please enter your message or enquiry</label>
                        <textarea class="form-control" name="message" placeholder="Message" rows="4"></textarea>
                        <div class="help-block">
                            {{$errors->first('message')}}
                        </div>
                    </div>
                    <button class="btn-blackmain btn-block" name="save" type="submit">Send Message</button>
                    <?= Form::close() ?>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@stop
@section('js')
  @include('admin.layout.alert')
<script type="text/javascript">
    $(".number_only").keypress(function(h){if(8!=h.which&&0!=h.which&&(h.which<48||h.which>57))return!1});
</script>
@stop