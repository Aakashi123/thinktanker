<!--quicksection start here-->
    <section class="homequick-facts">
      <div class="container">
        <h2 class="main-title quick-facts-title text-center"><span class="second">Quick </span>Facts</h2>
        <div class="row">
          <div class="col-sm-3 quick-facts-row text-center">
            <div class="fact-img"><img title="" alt="" src=<?= IMAGE_PATH."/frontend/images/thumb-up_white.png" ?>></div>
            <div class="fact-main"><span class="counter" data-count="25"></span><span class="fact-symbol">+</span></div>
            <div class="fact-title">IT Experts</div>
          </div>
          <div class="col-sm-3 quick-facts-row text-center">
            <div class="fact-img"><img title="" alt="" src=<?= IMAGE_PATH."/frontend/images/rocket_white.png" ?>></div>
            <div class="fact-main"><span class="counter" data-count="50"></span><span class="fact-symbol">+</span></div>
            <div class="fact-title">Projects Completed</div>
          </div>
          <div class="col-sm-3 quick-facts-row text-center">
            <div class="fact-img"><img title="" alt="" src=<?= IMAGE_PATH."/frontend/images/bag_white.png"?>></div>
            <div class="fact-main"><span class="counter" data-count="30"></span><span class="fact-symbol">+</span></div>
            <div class="fact-title">Satisfied Clients</div>
          </div>
          <div class="col-sm-3 quick-facts-row text-center">
            <div class="fact-img"><img title="" alt="" src=<?= IMAGE_PATH."/frontend/images/coffee_white.png" ?>></div>
            <div class="fact-main"><span class="counter" data-count="6"></span><span class="fact-symbol">+</span></div>
            <div class="fact-title">Years Of Experience</div>
          </div>
        </div>
      </div>
    </section>
    <!--quicksection end here-->