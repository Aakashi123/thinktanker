@extends('frontend.layout.index')
@section('meta')
    <meta name="title" content="{{$general_data['meta_title']}}">
    <meta name="description" content="{{$general_data['meta_description']}}">
    <meta name="keywords" content="{{$general_data['meta_keyword']}}">
@stop
@section('dubline')
<meta name="dc.source" CONTENT="<?= config('app.url')."/".Request::path()?>">
    <meta name="dc.title" CONTENT="{{$general_data['meta_title']}}">
    <meta name="dc.description" CONTENT="{{$general_data['meta_description']}}">
    <meta name="dc.format" CONTENT="text/html">
    <meta name="dc.language" CONTENT="en">
@stop
@section('content')
	@include('frontend.homepage.baner')
	@include('frontend.homepage.service')
	@include('frontend.homepage.portfolio')
	@include('frontend.homepage.companypatner')
	@include('frontend.homepage.homequick')
	@include('frontend.homepage.testimonial')
@stop