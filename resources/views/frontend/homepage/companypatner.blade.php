<!--companypartner section start here -->
    <section class="section container-fulid logos-list-section">
      <div class="container">
        <div class="row align-items-center">
          <div class="col logos-list"><img class="img-fulid" src=<?= IMAGE_PATH."/frontend/images/company-logo1-gray.png" ?> alt=""></div>
          <div class="col logos-list"><img class="img-fulid" src=<?= IMAGE_PATH."/frontend/images/company-logo2-gray.png" ?> alt=""></div>
          <div class="col logos-list"><img class="img-fulid" src=<?= IMAGE_PATH."/frontend/images/company-logo3-gray.png" ?> alt=""></div>
          <div class="col logos-list"><img class="img-fulid" src=<?= IMAGE_PATH."/frontend/images/company-logo4-gray.png" ?> alt=""></div>
          <div class="col logos-list"><img class="img-fulid" src=<?= IMAGE_PATH."/frontend/images/company-logo5-gray.png" ?> alt=""></div>
          <div class="col logos-list"><img class="img-fulid" src=<?= IMAGE_PATH."/frontend/images/company-logo6-gray.png" ?> alt=""></div>
        </div>
      </div>
    </section>
    <!--companypartner section end here-->