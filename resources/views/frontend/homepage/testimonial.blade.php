<!--testimonials section start here-->
    <section class="section section-clients">
      <div class="container">
        <h2 class="main-title text-center"> <span class="second">What </span>Clients Say</h2>
        <div class="row">
          <div class="section-client slider" id="section-clients">
            <div>
              <div class="client-col">
                <div class="client-content">
                  <div class="client-img text-center"><img src=<?= IMAGE_PATH."/frontend/images/user1.png" ?>></div>
                  <p><i class="fas fa-quote-left pr-10"></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.<i class="fas fa-quote-right pl-10"></i></p>
                  <div class="client-name text-center"><span>Tejash shah</span></div>
                </div>
              </div>
            </div>
            <div>
              <div class="client-col">
                <div class="client-content">
                  <div class="client-img text-center"><img src=<?= IMAGE_PATH."/frontend/images/user1.png" ?>></div>
                  <p><i class="fas fa-quote-left pr-10"></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.<i class="fas fa-quote-right pl-10"></i></p>
                  <div class="client-name text-center"><span>Tejash shah</span></div>
                </div>
              </div>
            </div>
            <div>
              <div class="client-col">
                <div class="client-content">
                  <div class="client-img text-center"><img src=<?= IMAGE_PATH."/frontend/images/user1.png" ?>></div>
                  <p><i class="fas fa-quote-left pr-10"></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.<i class="fas fa-quote-right pl-10"></i></p>
                  <div class="client-name text-center"><span>Tejash shah</span></div>
                </div>
              </div>
            </div>
            <div>
              <div class="client-col">
                <div class="client-content">
                  <div class="client-img text-center"><img src=<?= IMAGE_PATH."/frontend/images/user1.png" ?>></div>
                  <p><i class="fas fa-quote-left pr-10"></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.<i class="fas fa-quote-right pl-10"></i></p>
                  <div class="client-name text-center"><span>Tejash shah</span></div>
                </div>
              </div>
            </div>
            <div>
              <div class="client-col">
                <div class="client-content">
                  <div class="client-img text-center"><img src=<?= IMAGE_PATH."/frontend/images/user1.png" ?>></div>
                  <p><i class="fas fa-quote-left pr-10"></i>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.<i class="fas fa-quote-right pl-10"></i></p>
                  <div class="client-name text-center"><span>Tejash shah</span></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--testimonials section end here-->