<!-- banner section start here-->
    <section class="banner-section">
      <div id="landing-cover">
        <div class="layer bglayer"></div>
        <canvas class="background"></canvas>
        <div class="layer frontlayer"></div>
      </div>
      <div class="overlay">
        <div class="container">
          <div class="row justify-content-end">
            <div class="col-8 type-wrap">
              <p class="home-banner-title">Design Develop Deliver</p>
              <hr>
              <p class="home-banner-text">We strive for excellence in web designing, development and digital marketing. We spend our time brightening up the world wide web.</p>
              <div class="align-button">
                <div class="button-wrap"><a class="main-btn" href="javascript:;">View Portfolio</a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- banner section end here-->