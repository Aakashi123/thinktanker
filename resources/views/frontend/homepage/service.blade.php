 <!--servicesection start here-->
    <section class="home-services">
        <div class="container">
            <h2 class="main-title text-center">Our Services</h2>
            <div class="row">
                @foreach($homepage_service_data as $data)
                    <div class="col-sm-6 col-md-4 service-col">
                        <div class="service">
                            <div class="icon-services">
                                <div class="service-icon text-center">
                                    <i class="fas fa-desktop"></i>
                                </div>
                            </div>
                            <div class="title-services">
                                <h3 class="service-title"><a href="<?= route('details.get',$data['slug'])?>">{{$data['home_title']}}</a></h3>
                            </div>
                            <div class="con-services">
                                <p class="service-con"><?= $data['home_description']?></p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!--servicesection end here-->