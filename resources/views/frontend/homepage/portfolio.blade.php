<!--portfolio section start here-->
    <section class="section home-portfolio">
      <h2 class="main-title text-center"> <span class="second">Our </span>Projects</h2>
      <div class="row">
        <div class="col-md-4 col-sm-6 portfolio-col">
          <div class="portfolio-example">
            <div class="portfolio-front"><img class="img-fluid" src=<?= IMAGE_PATH."/frontend/images/project1.jpg" ?> alt="project" title="project1">
              <div class="footer"><span class="title">Development of Clinical Guidelines</span></div>
            </div>
            <div class="portfolio-back">
              <h3 class="title">Development of Clinical Guidelines</h3>
              <div class="content-portfolio">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
              </div>
              <div class="footer"><a href="javascript:;"><span class="main-btn">Read More</span></a></div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-col">
          <div class="portfolio-example">
            <div class="portfolio-front"><img class="img-fluid" src=<?= IMAGE_PATH."/frontend/images/project2.jpg" ?> alt="project" title="project1">
              <div class="footer"><span class="title">Development of Clinical Guidelines</span></div>
            </div>
            <div class="portfolio-back">
              <h3 class="title">Development of Clinical Guidelines</h3>
              <div class="content-portfolio">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
              </div>
              <div class="footer"><a href="javascript:;"><span class="main-btn">Read More</span></a></div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-col">
          <div class="portfolio-example">
            <div class="portfolio-front"><img class="img-fluid" src=<?= IMAGE_PATH."/frontend/images/project3.jpg" ?> alt="project" title="project1">
              <div class="footer"><span class="title">Development of Clinical Guidelines</span></div>
            </div>
            <div class="portfolio-back">
              <h3 class="title">Development of Clinical Guidelines</h3>
              <div class="content-portfolio">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
              </div>
              <div class="footer"><a href="javascript:;"><span class="main-btn">Read More</span></a></div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-col">
          <div class="portfolio-example">
            <div class="portfolio-front"><img class="img-fluid" src=<?= IMAGE_PATH."/frontend/images/project4.jpg" ?> alt="project" title="project1">
              <div class="footer"><span class="title">Development of Clinical Guidelines</span></div>
            </div>
            <div class="portfolio-back">
              <h3 class="title">Development of Clinical Guidelines</h3>
              <div class="content-portfolio">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
              </div>
              <div class="footer"><a href="javascript:;"><span class="main-btn">Read More</span></a></div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-col">
          <div class="portfolio-example">
            <div class="portfolio-front"><img class="img-fluid" src=<?= IMAGE_PATH."/frontend/images/project1.jpg" ?> alt="project" title="project1">
              <div class="footer"><span class="title">Development of Clinical Guidelines</span></div>
            </div>
            <div class="portfolio-back">
              <h3 class="title">Development of Clinical Guidelines</h3>
              <div class="content-portfolio">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
              </div>
              <div class="footer"><a href="javascript:;"><span class="main-btn">Read More</span></a></div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-col">
          <div class="portfolio-example">
            <div class="portfolio-front"><img class="img-fluid" src=<?= IMAGE_PATH."/frontend/images/project2.jpg" ?> alt="project" title="project1">
              <div class="footer"><span class="title">Development of Clinical Guidelines</span></div>
            </div>
            <div class="portfolio-back">
              <h3 class="title">Development of Clinical Guidelines</h3>
              <div class="content-portfolio">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
              </div>
              <div class="footer"><a href="javascript:;"><span class="main-btn">Read More</span></a></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--portfolio section end here-->