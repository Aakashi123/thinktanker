@extends('admin.layout.index')
@section('content')
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
			    <div class="box-header with-border">
					<h3 class="box-title">Technology</h3>
				</div>
				<br>
				<?= Form::model($technology_data,['method'=>'PATCH','route'=>['technology.update',$id],'class'=>'form-horizontal','enctype'=>'multipart/form-data','files' => 'true'])?>
					<div class="box-body">
						<div class="form-group">
                            <label class="control-label col-md-2">Technology<span class="text-danger"> *</span></label>
                            <div class="col-md-8 @if($errors->has('technology_name')) has-error @endif">
                                <?= Form::text('technology_name',null,['class'=>'form-control','id'=>'technology_name','rows'=>'4']) ?> 
                                	<span class='text-danger error'>{{ $errors->first('technology_name') }}</span>
                            </div>
                        </div>
                    </div>         
					<div class="box-footer">
                        <div class="form-group">
                            <?= Form::label('','',['class'=>'col-sm-2 control-label'])?>
                            <div class="col-sm-8 text-right">
                                <?= Form::submit('Save & Exit',['class'=>'btn btn-primary btn-sm','title'=>'Save & Exit'])?>
                                <a href="{{ route('technology.index')}}" title="Back"><button type="button" class="btn btn-default btn-sm" name="cancel">Back</button></a>
                            </div>
                        </div>
                    </div>
				<?= Form::close()?>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('js')	
    @include('admin.layout.alert')
@stop
