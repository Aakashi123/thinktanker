@extends('admin.layout.index')
@section('content')
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
			    <div class="box-header with-border">
					<h3 class="box-title">Educational</h3>
				</div>
				<br>
				<?= Form::model($education_data,['method'=>'PATCH','route'=>['educational.update',$id],'class'=>'form-horizontal','enctype'=>'multipart/form-data','files' => 'true'])?>
					<div class="box-body">
						<div class="form-group">
                            <label class="control-label col-md-2">Select Qualification</label>
                            <div class="col-md-8 @if($errors->has('educational_name')) has-error @endif">
                                <?= Form::text('educational_name',null,['class'=>'form-control','id'=>'educational_name','rows'=>'4']) ?> 
                                	<span class='text-danger error'>{{ $errors->first('educational_name') }}</span>
                            </div>
                        </div>
                    </div>         
					<div class="box-footer">
                        <div class="form-group">
                            <?= Form::label('','',['class'=>'col-sm-2 control-label'])?>
                            <div class="col-sm-8 text-right">
                                <?= Form::submit('Save & Exit',['class'=>'btn btn-primary btn-sm','title'=>'Save & Exit'])?>
                                <a href="{{ route('educational.index')}}" title="Back"><button type="button" class="btn btn-default btn-sm" name="cancel">Back</button></a>
                            </div>
                        </div>
                    </div>
				<?= Form::close()?>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('js')	
    @include('admin.layout.alert')
@stop
