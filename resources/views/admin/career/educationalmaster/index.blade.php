@extends('admin.layout.index')
@section('content')
<section class="content">
        <h3>
            Educational
        </h3>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header blue-border">
                        <div class="row">
                            <h3 class="box-title col-md-6 col-xs-12">Educational Qualification</h3>
                            <div class="text-right dropdown js__drop_down col-md-6 col-xs-12">
                                <a href="<?=route('educational.create')?>" class="btn btn-primary btn-sm" title="Add New">Add New</a>
                                <a href="javascript:;" class="btn btn-danger delete_record btn-sm" title="Delete">Delete</a>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="master_table" class="table table-striped table-bordered display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="text-left">
                                            <div class="checkbox purple">
                                                <input type="checkbox" id="selectall1" class="select_check_box">
                                            </div>
                                        </th>
                                        <th>Educational Qualification</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th class="text-left">
                                            <div class="checkbox purple">
                                                <input type="checkbox" id="selectall1" class="select_check_box">
                                            </div>
                                        </th>
                                        <th>Educational Qualification</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>
@stop

@section('css')
{{ Html::style('backend/css/dataTables.bootstrap.css') }}
{{ Html::style('backend/css/dataTables.bootstrap.min.css') }}
{{ Html::style('backend/css/responsive.bootstrap.min.css') }}
{{ Html::style('backend/css/sweetalert.css') }}
@stop

@section('js')
{{ Html::script('backend/js/jquery.dataTables.min.js') }}
{{ Html::script('backend/js/dataTables.bootstrap.min.js') }}
{{ Html::script('backend/js/fnStandingRedraw.js') }}
{{ Html::script('backend/js/dataTables.responsive.min.js') }}
{{ Html::script('backend/js/sweetalert.min.js') }}
{{ Html::script("backend/js/delete_script.js") }}

<script type="text/javascript">

    var title = "Are you sure to delete selected record(s)?";
    var text = "You will not be able to recover this record";
    var type = "warning";
    var delete_path = "{{ URL::route('educational.delete')}}";
    var token = "{{ csrf_token() }}";

    $('.delete_record').click(function(){
        var delete_id = $('#master_table tbody input[type=checkbox]:checked');
        checkLength(delete_id);
    });

    $(document).ready(function()
    {
        var master = $('#master_table').dataTable({
            bProcessing: false,
            bServerSide: true,
            autoWidth: true,
            stateSave: true,
            aaSorting: [
            [1, 'desc']
            ],
            sAjaxSource: "{{ URL::route('educational.index')}}",
            fnServerParams: function ( aoData ) {
                aoData.push({ "name": "act", "value": "fetch" });
                server_params = aoData;
            },
            aoColumns: [
            {
                mData: "id",
                bSortable: false,
                bVisible: true,
                sWidth: "3%",
                sClass: 'text-center',
                mRender: function(v, t, o)
                {
                    return '<div class="checkbox purple">'
                    +'<input type="checkbox" id="chk_'+v+'" name="id[]" value="'+v+'"/>'
                    +'<label for="chk_'+v+'" style="min-height: 14px;></label>'
                    +'</div>';
                }
            },
            { "mData": "educational_name",sWidth: "10%",bSortable: true,},           
            
        {
            mData: null,
            bSortable: false,
            sWidth: "7%",
            sClass: "text-center",
            mRender: function(v, t, o) {

                var editurl = "{{ route('educational.edit',':id')}}";
                editurl = editurl.replace(':id',o['id']);


                var act_html = "<div class='btn-group'>"
                +"<a href='"+editurl+"'class='btn btn-default btn-sm'><i class='fa fa-pencil' title='Edit Record'></i></a>"
                +"<a href='javascript:void(0)' onclick=\"deleteRecord('"+delete_path+"','"+title+"','"+text+"','"+token+"','"+type+"',"+o['id']+")\"class='btn btn-default btn-sm'><i class='fa fa-trash' title='Delete Record'></a>"
                +"</div>"
                return act_html;
            }
        },
        ],
        fnPreDrawCallback : function() {
        },
        fnDrawCallback : function (oSettings) {
        }
    });

        master.fnSetFilteringDelay(1000);
    });
    $(".select_check_box").on('click', function(){
        var is_checked = $(this).is(':checked');
        $(this).closest('table').find('tbody tr td:first-child input[type=checkbox]').prop('checked',is_checked);
        $(".select_check_box").prop('checked',is_checked);
    });
</script>

@include('admin.layout.alert')
@stop
