@extends('admin.layout.index')
@section('content')
<section class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
			    <div class="box-header with-border">
					<h3 class="box-title">Create Career</h3>
				</div>
				<br>
				<?= Form::open(['method'=>'post','route'=>'career.store','class'=>'form-horizontal','enctype'=>'multipart/form-data','files' => 'true'])?>
					<div class="box-body">
						<div class="form-group">
							<div class="col-sm-2 text-right">
								<?= Form::label('title','',['class'=>'control-label'])?><span class="text-danger"> *</span>
							</div>
							<div class="col-sm-8  @if($errors->has('title')) has-error @endif">
								<?= Form::text('title','',['class'=>'form-control','id'=>'title','placeholder'=>'Enter Title','maxlength'=>'50'])?>
								<div class="help-block">
			                        {{$errors->first('title')}}
			                    </div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2 text-right">
								<?= Form::label('Slug','',['class'=>'control-label'])?>
							</div>
							<div class="col-sm-8  @if($errors->has('slug')) has-error @endif">
								<?= Form::text('slug','',['class'=>'form-control','id'=>'slug','placeholder'=>'Enter Slug','maxlength'=>'50'])?>
								<div class="help-block">
									{{$errors->first('slug')}}
								</div>
                                <strong><small id="passwordHelpBlock" class="form-text text-muted">
                                    (Will be automatically generated from your title,if left empty)
                                </small></strong>
							</div>
						</div> 
						<div class="form-group">
			                <label for="fun" class="col-sm-2 control-label">Active/NotActive Title</label>
			                    <div class="col-md-8">
			                        <div class="input-group">
			                            <div id="radioBtn" class="btn-group">
			                                <a class="btn btn-primary btn-sm active" data-toggle="happy" data-title="active" id="title_active">Active</a>
			                                <a class="btn btn-primary btn-sm notActive" data-toggle="happy" data-title="notactive" id="title_notactive">Not Active</a>
			                            </div>
			                            <input type="hidden" name="activetitle" id="titlehidden" value="">
			                        </div>
			                        <span class='text-danger error'>{{ $errors->first('activetitle') }}</span>
			                    </div>
			                </div>
			                <div class="form-group">
                            	<label class="control-label col-md-2">Description<span class="text-danger"> *</span></label>
                            	<div class="col-md-8 @if($errors->has('description')) has-error @endif">
                                	<?= Form::textarea('description',old('description'),['class'=>'form-control','id'=>'editor1','rows'=>'4']) ?> 
                                	<span class='text-danger error'>{{ $errors->first('description') }}</span>
                            	</div>
                        	</div>
                        <div class="form-group">
                            <div class="col-sm-2 text-right">
                                <?= Form::label('Meta Title','',['class'=>'control-label'])?>
                            </div>
                            <div class="col-sm-8  @if($errors->has('metatitle')) has-error @endif">
                                <?= Form::text('metatitle','',['class'=>'form-control','id'=>'metatitle','placeholder'=>'Enter MetaTitle','maxlength'=>'50'])?>
                                <span class='text-danger error'>{{ $errors->first('metatitle') }}</span>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-2">Meta Description</label>
                            <div class="col-md-8 @if($errors->has('metadescription')) has-error @endif">
                                <?= Form::textarea('metadescription',old('metadescription'),['class'=>'form-control','id'=>'metadescription','rows'=>'4','placeholder'=>'Enter Meta Description']) ?> 
                                    <span class='text-danger error'>{{ $errors->first('metadescription') }}</span>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="control-label col-md-2">Meta Keyword</label>
                            <div class="col-md-8 @if($errors->has('metakeyword')) has-error @endif">
                                <?= Form::textarea('metakeyword',old('metakeyword'),['class'=>'form-control','id'=>'metakeyword','rows'=>'4','placeholder'=>'Enter Meta Keyword']) ?> 
                                    <span class='text-danger error'>{{ $errors->first('metakeyword') }}</span>
                            </div>
                        </div>  
                    </div>
                    <div class="box-footer">
						<div class="form-group">
							<?= Form::label('','',['class'=>'col-sm-2 control-label'])?>
							<div class="col-sm-8 text-right">
								<button class="btn btn-primary" title="save" type="submit" name="save" value="add">Save</button>
								<button class="btn btn-primary" title="save & new" type="submi" name="save" value="new">Save & Create New Record</button>
								<a href="<?=URL::route('career.index')?>" title='Back'><button type="button" class="btn btn-default btn-sm" name="cancel">Back</button></a>
							</div>
						</div>       
					</div>
					<?= Form::close()?>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('css')
<style type="text/css">
    #radioBtn .notActive{
        color: #3276b1;
        background-color: #fff;
    }
    #radioBtn1 .notActive{
        color: #3276b1;
        background-color: #fff;
    }
</style>
@stop
@section('js')	
{{ Html::script("http://cdn.ckeditor.com/4.7.1/standard-all/ckeditor.js") }}
<script type="text/javascript">
	$('#radioBtn a').on('click', function(){
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#'+tog).prop('value', sel);
        $('#titlehidden').attr('value',sel);
        
        $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    });
    $('#radioBtn1 a').on('click', function(){
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#'+tog).prop('value', sel);
        $('#subtitlehidden').attr('value',sel);
        
        $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    });
    var data = "{{old('activetitle')}}";
    if(data == 'enable')
    {
        $("#title_active").addClass('active');
        $("#title_active").removeClass('notActive');
        $("#title_notactive").addClass('notActive');
        $("#title_notactive").removeClass('active');
    }
    else if(data == 'disable')
    { 
        $("#title_notactive").addClass('active');
    }
    $('#tbName').on('input change', function () {
            if ($(this).val() != '') {
                $('#submit').prop('disabled', false);
            }
            else {
                $('#submit').prop('disabled', true);
            }
        });

    //ckeditor...............
    CKEDITOR.config.extraPlugins = 'justify';
    CKEDITOR.replace( 'editor1', {
        height: 300,

    // Configure your file manager integration. This example uses CKFinder 3 for PHP.
    filebrowserImageBrowseUrl: '/admin/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/admin/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/admin/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/admin/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}',


    toolbar: [
    { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
    { name: 'paragraph',  items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
    { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
    // { name: 'alignment', items : [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
    { name: 'clipboard',items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo']},
    '/',
    { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
    { name : 'links',items : ['Link','Unlink','Anchor']},
    { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
    { name: 'document', items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
    { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'others', items: [ '-' ] },
    ],
    removeButtons: '',
    image_previewText: ' '
});
</script>
@include('admin.layout.alert')

@stop
