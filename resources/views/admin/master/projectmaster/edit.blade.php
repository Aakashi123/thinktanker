@extends('admin.layout.index')

@section('content')
<section class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="box blue-border">
					<div class="box-header with-border">
						<h3 class="box-title">Edit Project Information - {{$projectmaster_data['title']}}</h3>
					</div>
					<br>
					<?= Form::model($projectmaster_data,['method'=>'PATCH','route'=>['projectmaster.update',$id],'class'=>'form-horizontal','files'=>'true'])?>
					<div class="box-body">
						<div class="form-group">
							<div class="col-sm-2 text-right">
								<?= Form::label('project Title','',['class'=>'control-label'])?><span class="text-danger"> *</span>
							</div>
							<div class="col-sm-8  @if($errors->has('title')) has-error @endif">
								<?= Form::text('title',null,['class'=>'form-control','id'=>'name','placeholder'=>'Enter Project Title','maxlength'=>'50'])?>
								<div class="help-block">
									{{$errors->first('title')}}
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2 text-right">
								<?= Form::label('Slug','',['class'=>'control-label'])?>
							</div>
							<div class="col-sm-8  @if($errors->has('slug')) has-error @endif">
								<?= Form::text('slug',null,['class'=>'form-control','id'=>'slug','placeholder'=>'Enter Slug','maxlength'=>'50'])?>
								<div class="help-block">
									{{$errors->first('slug')}}
								</div>
								<strong><small id="passwordHelpBlock" class="form-text text-muted">
									(Will be automatically generated from your title,if left empty)
								</small></strong>
							</div>
						</div>
						<div class="form-group">
                            	<label class="control-label col-md-2">Project Description<span class="text-danger"> *</span></label>
                            	<div class="col-md-8 @if($errors->has('description')) has-error @endif">
                                	<?= Form::textarea('description',old('description'),['class'=>'form-control','id'=>'editor1','rows'=>'4']) ?> 
                                	<span class='text-danger error'>{{ $errors->first('description') }}</span>
                            	</div>
                        </div>
						<div class="form-group">
							<div class="col-sm-2 text-right">
								<?= Form::label('Featured Image','',['class'=>'control-label'])?><span class="text-danger"> *</span>
							</div>
							<div class="form-group col-sm-6">
								<div class="fileupload fileupload-new col-md-8" data-provides="fileupload">
									<div class="fileupload-new thumbnail img-responsive" style="width: 200px">
										@if(empty($projectmaster_data['image']))
										<img src="http://via.placeholder.com/100x100" alt="" style="width:200px;height:200px" />
										@else
										<img src="<?=IMAGE_PATH."/images/projectmaster/".$projectmaster_data['image']?>" alt="" style="width:200px;height:200px" />
										@endif
									</div>
									<div class="fileupload-preview fileupload-exists thumbnail " style="width: 200px;  line-height: 20px;">
									</div>
									<div>
										<label class="btn btn-file control-label btn-xs">
											<?=Form::file('image', ['class' => 'control-label col-md-2'])?>
											<span id="" class="fileupload-new btn-lg"><i class="fa fa-paper-clip"></i>Select Image</span>
											<span id="" class="fileupload-exists btn-lg"><i class="fa fa-undo"></i>Change</span>
										</label>
										<a href="" class="btn btn-danger fileupload-exists btn-md" data-dismiss="fileupload"><i class="fa fa-trash"></i>Remove</a>
										<br/><span class='text-danger error'>{{ $errors->first('image') }}</span>
									</div>
									<small id="passwordHelpBlock" class="form-text text-muted">
										The image dimension must be 200x200.
									</small>
								</div>
							</div>
						</div>
                        <div class="form-group">
							<div class="col-sm-2 text-right">
								<?= Form::label('Meta Title','',['class'=>'control-label'])?>
							</div>
							<div class="col-sm-8  @if($errors->has('metatitle')) has-error @endif">
								<?= Form::text('metatitle',null,['class'=>'form-control','id'=>'name','placeholder'=>'Enter Meta Title','maxlength'=>'50'])?>
								<div class="help-block">
									{{$errors->first('metatitle')}}
								</div>
							</div>
						</div>
						<div class="form-group">
                            	<label class="control-label col-md-2">Meta Description</label>
                            	<div class="col-md-8 @if($errors->has('metadescription')) has-error @endif">
                                	<?= Form::textarea('metadescription',old('metadescription'),['class'=>'form-control','id'=>'metadescription','rows'=>'4','placeholder'=>'Enter Meta Description']) ?> 
                                	<span class='text-danger error'>{{ $errors->first('metadescription') }}</span>
                            	</div>
                        </div>
                        <div class="form-group">
                            	<label class="control-label col-md-2">Meta Keyword</label>
                            	<div class="col-md-8 @if($errors->has('metakeyword')) has-error @endif">
                                	<?= Form::textarea('metakeyword',old('metadescription'),['class'=>'form-control','id'=>'metakeyword','rows'=>'4','placeholder'=>'Enter Meta Keyword']) ?> 
                                	<span class='text-danger error'>{{ $errors->first('metakeyword') }}</span>
                            	</div>
                        </div>
						<div class="form-group">
							<div class="col-sm-2 text-right">
								<?= Form::label('Link','',['class'=>'control-label'])?><span class="text-danger"> *</span>
							</div>
							<div class="col-sm-8  @if($errors->has('link')) has-error @endif">
								<?= Form::text('link',null,['class'=>'form-control','id'=>'link','placeholder'=>'Enter Link','maxlength'=>'50'])?>
								<div class="help-block">
									{{$errors->first('link')}}
								</div>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div class="form-group">
							<?= Form::label('','',['class'=>'col-sm-2 control-label'])?>
							<div class="col-sm-8 text-right">
								<?= Form::submit('Save',['class'=>'btn btn-primary btn-sm','name'=>'save','title'=>'Save Record'])?>
								<?= Form::submit('Save & Exit',['class'=>'btn btn-primary btn-sm','title'=>'Save & Exit'])?>
								<a href="<?=URL::route('projectmaster.index')?>" title="Back"><button type="button" class="btn btn-default btn-sm" name="cancel">Back</button></a>
							</div>
						</div>
					</div>
					<?= Form::close()?>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('css')
{{ Html::style("backend/css/bootstrap-fileupload.css") }}
@stop

@section('js')
{{ Html::script("backend/js/bootstrap-fileupload.js") }}
{{ Html::script("http://cdn.ckeditor.com/4.7.1/standard-all/ckeditor.js") }}
<script type="text/javascript">
CKEDITOR.config.extraPlugins = 'justify';
    CKEDITOR.replace( 'editor1', {
        height: 300,

    // Configure your file manager integration. This example uses CKFinder 3 for PHP.
    filebrowserImageBrowseUrl: '/admin/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/admin/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/admin/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/admin/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}',


    toolbar: [
    { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
    { name: 'paragraph',  items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
    { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
    // { name: 'alignment', items : [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
    { name: 'clipboard',items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo']},
    '/',
    { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
    { name : 'links',items : ['Link','Unlink','Anchor']},
    { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
    { name: 'document', items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
    { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'others', items: [ '-' ] },
    ],
    removeButtons: '',
    image_previewText: ' '
});
</script>
@include('admin.layout.alert')
@stop