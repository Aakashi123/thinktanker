@extends('admin.layout.index')
@section('content')
<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box blue-border">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create Project Gallery</h3>
                    </div>
                    <br>
                    <?= Form::open(['route' => 'projectgallery.store','method'=>'POST','class'=>'form-horizontal','files'=>'true'])?>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-2 text-right">
                                <?= Form::label('title','',['class'=>'control-label'])?><span class="text-danger"> *</span>
                            </div>
                            <div class="col-sm-8  @if($errors->has('title')) has-error @endif">
                                <?= Form::text('title','',['class'=>'form-control','id'=>'title','placeholder'=>'Enter Title','maxlength'=>'50'])?>
                                <div class="help-block">
                                    {{$errors->first('title')}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2 text-right">
                                <?= Form::label('featured image','',['class'=>'control-label'])?><span class="text-danger"> *</span>
                            </div>
                            <div class="col-sm-10 @if($errors->has('image')) {{ 'has-error' }} @endif">
                                <input type="file" name="image" data-fileuploader-files='' class="multi_image">
                                <span id="image_error" class="help-inline text-danger"><?=$errors->first('image')?></span>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="form-group">
                            <?= Form::label('','',['class'=>'col-sm-2 control-label'])?>
                            <div class="col-sm-8 text-right"> 
                                <?= Form::submit('Save',['class'=>'btn btn-primary btn-sm','title'=>'Save Record'])?>
                                <?= Form::submit('Save & New',['class'=>'btn btn-primary btn-sm','name'=>'save_new','title'=>'Save & Create New Record'])?>
                                <a href="{{ route('projectgallery.index') }}" title="Back"><button type="button" class="btn btn-default btn-sm" name="cancel">Back</button></a>
                            </div>
                        </div>
                    </div>
                    <?= Form::close()?>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('css')
<?=Html::style('backend/css/jquery.fileuploader.css')?>
<?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
@stop

@section('js')
<?=Html::script('backend/js/jquery.fileuploader.min.js')?>
<script type="text/javascript">
        $(document).ready(function(){

          var exists_image = '';
          console.log(exists_image);
    $('.multi_image').attr('data-fileuploader-files',exists_image);

        $('input[name="image"]').fileuploader({
        extensions: ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
        changeInput: ' ',
        theme: 'thumbnails',
        enableApi: true,
        addMore: true,
        limit:10,

        thumbnails: {
            box: '<div class="fileuploader-items">' +
                      '<ul class="fileuploader-items-list">' +
                          '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner" title="Add image">+</div></li>' +
                      '</ul>' +
                  '</div>',
            item: '<li class="fileuploader-item">' +
                       '<div class="fileuploader-item-inner">' +
                           '<div class="thumbnail-holder">${image}</div>' +
                           '<div class="actions-holder">' +
                               '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
                           '</div>' +
                           '<div class="progress-holder">${progressBar}</div>' +
                       '</div>' +
                   '</li>',
            item2: '<li class="fileuploader-item">' +
                       '<div class="fileuploader-item-inner">' +
                           '<div class="thumbnail-holder">${image}</div>' +
                           '<div class="actions-holder">' +
                               '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
                           '</div>' +
                       '</div>' +
                   '</li>',
            startImageRenderer: true,
            canvasImage: false,
            _selectors: {
                list: '.fileuploader-items-list',
                item: '.fileuploader-item',
                start: '.fileuploader-action-start',
                retry: '.fileuploader-action-retry',
                remove: '.fileuploader-action-remove'
            },
            onItemShow: function(item, listEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input');

                plusInput.insertAfter(item.html);

                if(item.format == 'image') {
                    item.html.find('.fileuploader-item-icon').hide();
                }
            }
        },
        afterRender: function(listEl, parentEl, newInputEl, inputEl) {
            var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                api = $.fileuploader.getInstance(inputEl.get(0));
            plusInput.on('click', function() {
                api.open();
            });
        },
        
    });
});
</script>
@include('admin.layout.alert')
@stop