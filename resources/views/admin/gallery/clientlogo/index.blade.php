@extends('admin.layout.index')

@section('content')
<section class="content">
    <div class="container">
        <h3>
            Client Logo
        </h3>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header blue-border">                        
                        <div class="row">
                            <h3 class="box-title col-md-6 col-xs-12">Client Logos</h3>
                            <div class="text-right dropdown js__drop_down col-md-6 col-xs-12">
                                @if(!count($clientlogo_data))
                                <a href="{{ route('clientlogo.create')}}" id="addnew" class="btn btn-primary btn-sm" title="Add New">Add New</a>
                                @else
                                <a href="<?=route('clientlogo.edit') ?>" class="btn btn-primary btn-sm" id="edit" title="Edit">Edit</a>
                                @endif
                            </div>
                        </div>
                        @foreach($clientlogo_data as $single_data)
                            <div class="col-sm-3" style="margin-bottom: 30px">
                                <img src="<?=IMAGE_PATH."/images/clientlogo/".$single_data['image_name']?>" width="200px" height="160px"  />
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('js')
@stop
                   
