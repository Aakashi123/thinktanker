@extends('admin.layout.index')

<!-- @section('css')
<?=Html::style('backend/css/jquery.fileuploader.css')?>
<?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
@stop
 -->
@section('content')
<section class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
				    <div class="box-header with-border">
						<h3 class="box-title">Select Client Logos</h3>
					</div>
					<br>
					<?=Form::open(['route'=>'clientlogo.store','files' => true, 'method' => 'post'])?>
					<div class="box-body">
						<div class="form-group">
              <div class="form-group @if($errors->has('files')) {{ 'has-error' }} @endif">
                <label>Select Image(1500*500) size</label>
                <input type="file" id="fileupload" name="files" class="multi_image">
                <!-- class="multi_image"  -->
                <span id="files_error" class="help-inline text-danger"><?=$errors->first('files')?></span>
                <div class="validation" style="display:none;"> Upload Max 4 Files allowed </div>
              </div>
						</div>
					</div>
					<div class="box-footer">
							<div class="form-group">
								<?= Form::label('','',['class'=>'col-sm-2 control-label'])?>
								<div class="col-sm-8 text-right">
									<button class="btn btn-primary" type="submit" name="save" value="add">Save</button>
									<a href="<?=URL::route('testimonial.index')?>" title='Back'><button type="button" class="btn btn-default btn-sm" name="cancel">Back</button></a>
								</div>
							</div>       
						</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('js')


<script type="text/javascript">
    $(document).ready(function(){
		$('#fileupload').fileuploader({
            extensions: ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
            changeInput: ' ',
            theme: 'thumbnails',
            enableApi: true,
            addMore: true,
            limit:10,
            thumbnails: {
                box: '<div class="fileuploader-items">' +
                          '<ul class="fileuploader-items-list">' +
                              '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner" title="Add image">+</div></li>' +
                          '</ul>' +
                      '</div>',
                item: '<li class="fileuploader-item">' +
                           '<div class="fileuploader-item-inner">' +
                               '<div class="thumbnail-holder">${image}</div>' +
                               '<div class="actions-holder">' + '<a class="fileuploader-action fileuploader-action" title="Edit"><i class="i class="far fa-edit""></i></a>' +
                                   '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' 
                                    +
                               '</div>' +
                               '<div class="progress-holder">${progressBar}</div>' +
                           '</div>' +
                       '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove',
                },
                onItemShow: function(item, listEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input');

                    plusInput.insertAfter(item.html);

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                }
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = $.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },
        });
    });
</script>
@include('admin.layout.alert')
@stop