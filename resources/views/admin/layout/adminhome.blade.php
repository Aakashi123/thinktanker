<header class="fixed-header">
    <div class="header-top">
        <div class="container">
            <div class="pull-left">
                <a href="" class="logo">Laravel</a>
            </div>
            <!-- /.pull-left -->
            <div class="pull-right">
                <div class="ico-item">
                    <a href="#" class="ico-item fa fa-user js__toggle_open" data-target="#user-status"></a>
                    <div id="user-status" class="user-status js__toggle">
                        <a href="{{ route('admin.logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                    <!-- /#user-status -->
                </div>
                <!-- /.ico-item -->
            </div>
            <!-- /.pull-right -->
        </div>
        <!-- /.container -->
    </div>


<nav class="nav-horizontal">
    <div class="container">
        
</nav>
</header>
