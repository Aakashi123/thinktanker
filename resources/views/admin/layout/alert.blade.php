
<?= Html::style('backend/css/toastr.min.css') ?>
<?= Html::script('backend/js/toastr.min.js') ?>

@if(Session::has('message'))
  @if('success' == Session::get('message_type'))
    <script type="text/javascript">
      toastr.success("{{ Session::get('message') }}");
    </script>
  @endif
@endif

@if($errors->any())
    <script type="text/javascript">
        $(document).ready(function(){
            toastr.error("<b>There were some errors.</b>");
        });
    </script>
@endif