<!-- jQuery 3 -->
<?= Html::script('backend/js/jquery.min.js') ?>

<!-- Bootstrap 3.3.7 -->
<?= Html::script('backend/js/bootstrap.min.js') ?>
<!-- Slimscroll -->
<?= Html::script('backend/js/jquery.slimscroll.min.js') ?>
<!-- FastClick -->
<?= Html::script('backend/js/fastclick.js') ?>
<!-- AdminLTE App -->
<?= Html::script('backend/js/adminlte.min.js') ?>
<!-- AdminLTE for demo purposes -->
<?= Html::script('backend/js/demo.js') ?>

<?= Html::script('backend/js/bootstrap-fileupload.js') ?>

<?=Html::script('backend/js/jquery.fileuploader.min.js')?>

<?= Html::script('backend/js/jquery.inputmask.js') ?>
<?= Html::script('backend/js/jquery.inputmask.extensions.js') ?>
<?= Html::script('backend/js/icheck.min.js') ?>
<?= Html::script('backend/js/select2.full.min.js') ?>
<?= Html::script('backend/js/moment.min.js') ?>
<?= Html::script('backend/js/daterangepicker.js') ?>
<?= Html::script('backend/js/bootstrap-colorpicker.min.js') ?>

