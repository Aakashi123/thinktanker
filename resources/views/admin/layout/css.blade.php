  <!-- Bootstrap 3.3.7 -->
  <?=Html::style('backend/css/bootstrap.min.css')?>
  <!-- Font Awesome -->
  <?=Html::style('backend/css/font-awesome/css/font-awesome.min.css')?>
  <!-- Ionicons -->
  <?=Html::style('backend/css/ionicons.min.css')?>

  <?=Html::style('backend/css/dataTables.bootstrap.min.css')?>
  <!-- Theme style -->
  <?=Html::style('backend/css/AdminLTE.min.css')?>
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <?=Html::style('backend/css/_all-skins.min.css')?>
  <!-- Morris chart -->
  <?=Html::style('backend/css/morris.css')?>
  <!-- jvectormap -->
  <!-- Date Picker -->
  <?=Html::style('backend/css/bootstrap-datepicker.min.css')?>
  <!-- Daterange picker -->
  <?=Html::style('backend/css/daterangepicker.css')?>
  <!-- bootstrap wysihtml5 - text editor -->
  <?=Html::style('backend/css/bootstrap3-wysihtml5.min.css')?>

  <?=Html::style('backend/css/jquery.fileuploader.css')?>

<?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>

  <?=Html::style('backend/css/bootstrap-fileupload.css')?>
  
  <?=Html::style('backend/css/jquery-jvectormap.css')?>

  <?=Html::style('backend/css/custome.css')?>

<?=Html::style('backend/css/bootstrap-colorpicker.min.css')?>

<?=Html::style('backend/css/select2.min.css')?>


  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
