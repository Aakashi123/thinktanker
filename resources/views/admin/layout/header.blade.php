  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="{{ route('homepage.index')}}" class="navbar-brand">thinkTANKER</a>
          </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="@if(Request::segment(2) == 'menu') active @endif"><a href="{{route('menu.get')}}">Menu<span class="sr-only">(current)</span></a></li>

            <!-- Home Menu -->
            <li class="dropdown @if(Request::segment(2) == 'slider' || Request::segment(2) == 'generalinfo' || Request::segment(2) == 'testimonial') active @endif">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Homepage<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ route('slider.index') }}">Slider</a></li>
                <li><a href="{{ route('testimonial.index') }}">Testimonial</a></li>
                <li><a href="{{ route('generalinfo.index') }}">General Info</a></li>
              </ul>
            </li>

            <!-- Master Menu -->
            <li class="dropdown @if(Request::segment(2) == 'projectmaster' || Request::segment(2) == 'cms' ) active @endif">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Master<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ route('projectmaster.index') }}">Project Master</a></li>
                <li><a href="{{ route('cms.index') }}">CMS Master</a></li>   
              </ul>
            </li>

            <!-- Services Menu -->
            <li class="dropdown @if(Request::segment(2) == 'servicemaster' || Request::segment(2) == 'servicepart') active @endif">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ route('servicemaster.index')}}">Service Master</a></li>
                <li><a href="{{ route('servicepart.index')}}">Service Inner Part</a></li>    
              </ul>
            </li>

            <!-- Career Menu -->
            <li class="dropdown @if(Request::segment(2) == 'career' || Request::segment(2) == 'technology' || Request::segment(2) == 'educational') active @endif">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Career<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ route('career.index') }}">Career Master</a></li>
                <li><a href="{{ route('technology.index') }}">Technology Master</a></li>
                <li><a href="{{ route('educational.index') }}">Educational Master</a></li>    
              </ul>
            </li>

            <!-- Gallery Menu -->
            <li class="dropdown @if(Request::segment(2) == 'clientlogo' || Request::segment(2) == 'projectgallery')active @endif">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gallery<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ route('clientlogo.index') }}">Client Logos</a></li>
                <li><a href="{{ route('projectgallery.index') }}">Project Gallery</a></li>
              </ul>
            </li>
          </ul>
        </div>

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="dropdown messages-menu">
            <!-- Tasks Menu -->
            <li class="dropdown tasks-menu">
              
              <ul class="dropdown-menu">
                  <ul class="menu">
                    </li>
                    <!-- end task item -->
                  </ul>
                </li>
              </ul>
            </li>
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="{{ route('admin.logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
              </a>
              <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
              </form>
          </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>