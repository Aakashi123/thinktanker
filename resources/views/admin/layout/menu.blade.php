@extends('admin.layout.index')

@section('content')

<section class="content">
	<div class="col-md-12">
		<div class="box blue-border">
			<div class="box-header with-border">
				<h3 class="box-title">Create Menu</h3>
			</div>
			<div class="box-body menu-admin">
				{!! Menu::render() !!}
			</div>
		</div>
	</div>
</section>
@stop

@section('css')
<style type="text/css">
	.menu-admin .content{
		min-height: auto;
		padding: 0
	}
</style>
@stop

@section('js')
{!! Menu::scripts('backend/js/jquery_menu.min.js') !!}
@stop