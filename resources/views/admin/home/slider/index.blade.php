@extends('admin.layout.index')

@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <h3>
                Slider Manager
            </h3>
        </div>
        <div class="col-md-10">
            <div id="div_alert">
                @if(count($slider_data)>0)
                <div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button><strong>Note :</strong> Drag and reorder the images to change the order sequence.
                </div>
                @else
                <div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" title="Add Slider"><span aria-hidden="true">×</span></button>Alert! No images added to home page slider.
                </div>
                @endif
            </div>
        </div>
        <div class="col-md-2 mtb-10 text-right">
            <a data-showloading="yes" href="<?=URL::route('slider.create')?>" class="btn btn-primary" title="Add Slider"> Add Slider Image </a>
        </div>
    </div>
    @if(count($slider_data)>0)
    <div class="row js__sortable ui-sortable" id="sortable">
        @foreach($slider_data as $data)

        <div class="col-md-4" id="{{$data->id}}">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">{{$data->title}}</h3>
                    <span class="controls pull-right">
                        <a data-showloading="yes" href="<?= URL::route('slider.edit',['id'=>$data->id])?>"" type="button" class="control fa fa-pencil js__card_plus" title="Edit Slider"></a>
                        <a data-showloading="yes" href='javascript:void(0);' onclick="DeleteRecord(<?=$data->id?>)" type="button" class="control fa fa-close  js__card_plus" title="Delete Slider"></a>
                    </span>
                </div>
                <div class="box-body no-padding">
                    <img class="img-responsive" src="<?=asset('/images/slider/'.$data['image'])?>">
                </div>
            </div>
        </div>
        @endforeach  
    </div>
    @endif
</section>
@stop

@section('css')
{{ Html::style('backend/css/jquery-ui.min.css') }}
{{ Html::style('backend/css/jquery-ui.structure.min.css') }}
{{ Html::style('backend/css/jquery-ui.theme.min.css') }}
{{ Html::style('backend/css/sweetalert.css') }}
@stop

@section('js')
{{ Html::script('backend/js/jquery.mCustomScrollbar.concat.min.js') }}
{{ Html::script('backend/js/nprogress.js') }}
{{ Html::script('backend/js/jquery-ui.min.js')}}
{{ Html::script('backend/js/main.min.js')}}
{{ Html::script('backend/js/sweetalert.min.js') }}

<script type="text/javascript">
    var token = "<?=csrf_token()?>";
    function DeleteRecord(id){

        swal({
            title: "Are you sure to delete selected record(s)?",
            text: "It will delete this Slider record and can not be reverted",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, Delete it!",
            cancelButtonText: "No, Cancel it!",
            closeOnConfirm: true,
            closeOnCancel: true },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: "{{ URL::route('slider.delete')}}",
                        type:'post',
                        dataType:'json',
                        data:{ id : id,_token: token},
                        beforeSend:function(){
                            $('#spin').show();
                            window.location.reload();

                        },
                        complete:function(){
                            $('#spin').hide();
                        },
                        success: function (respObj) {
                            if(respObj){
                                toastr.success('Slider Successfully Deleted','Success!');
                                window.location.reload();
                            }
                        }
                    });
                }
            });
    }
    $(document).ready(function(){
        $( "#sortable" ).sortable({
            update: function(){ 
                var order = $(this).sortable('toArray',{attribute:'id' });
               // console.log(order);
                $.ajax({
                    url: "{{ URL::route('slider.order') }}",
                    type: 'post',
                    data: { order: order,_token:token },
                    complete: function(){
                        toastr.success('Sliders images have been reordered successfully.');
                    }
                });
            }
        });
        $( "#sortable" ).disableSelection();
    });      
</script>
@include('admin.layout.alert')
@stop