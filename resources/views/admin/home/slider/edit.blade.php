@extends('admin.layout.index')

@section('content')
<section class="content">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Edit Slider - {{$slider_data['title']}}</h3>
				</div>
				<?= Form::model($slider_data,['method'=>'PATCH','route'=>['slider.update',$slider_data->id],'class'=>'form-horizontal','enctype'=>'multipart/form-data'])?>
				<div class="box-body">
					<div class="form-group">
						<div class="col-sm-2 text-right">
							<?= Form::label('title','',['class'=>'control-label'])?><span class="text-danger"> *</span>
						</div>
						<div class="col-sm-8 @if($errors->has('title')) has-error @endif">
							<?= Form::text('title',null,['class'=>'form-control','id'=>'title','placeholder'=>'Title','maxlength'=>'50'])?>
							<div class="help-block">
								{{$errors->first('title')}}
							</div>
						</div>
					</div>
					<div class="form-group">
                        <label for="fun" class="col-sm-2 control-label">Enable/Disable Title</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                 <div id="radioBtn" class="btn-group">
		                                <a class="btn btn-primary btn-sm <?=(old('enabletitle',$slider_data['enabletitle']))=='enable'? 'active':'notActive' ?>" data-toggle="happy" data-title="enable" id="title_enable">Enable</a>
                                    <a class="btn btn-primary btn-sm <?=(old('enabletitle',$slider_data['enabletitle']))=='disable'? 'active':'notActive' ?>" data-toggle="happy" data-title="disable" id="title_disable">Disable</a>
		                            </div>
                                <?php
                                $slider_data_title = ($slider_data['enabletitle']== 'enable')?'enable':'disable';
                                ?>
                                <input type="hidden" name="enabletitle" id="titlehidden" value="<?= old('enabletitle',$slider_data_title)?>">
                            </div>
                            <div class="help-block" style="color: #a94442">
                                {{$errors->first('enabletitle')}}
                            </div>
                        </div>
                    </div>
					<div class="form-group">
						<div class="col-sm-2 text-right">
							<?= Form::label('subtitle','',['class'=>'control-label'])?><span class="text-danger"> *</span>
						</div>
						<div class="col-sm-8 @if($errors->has('subtitle')) has-error @endif">
							<?= Form::text('subtitle',null,['class'=>'form-control','id'=>'subtitle','placeholder'=>'SubTitle','maxlength'=>'100'])?>
							<div class="help-block">
								{{$errors->first('subtitle')}}
							</div>
						</div>
					</div>
					<div class="form-group">
                        <label for="fun" class="col-sm-2 control-label">Enable/Disable Sub Title</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <div id="radioBtn1" class="btn-group">
		                                <a class="btn btn-primary btn-sm <?=(old('enablesubtitle',$slider_data['enablesubtitle']))=='enable'? 'active':'notActive' ?>" data-toggle="happy1" data-title="enable" id="subtitle_enable">Enable</a>
                                    <a class="btn btn-primary btn-sm <?=(old('enablesubtitle',$slider_data['enablesubtitle']))=='disable'? 'active':'notActive' ?>" data-toggle="happy1" data-title="disable" id="subtitle_disable">Disable</a>
		                            </div>
		                        <?php
                                $slider_data_subtitle = ($slider_data['enablesubtitle']== 'enable')?'enable':'disable';
                                ?>
                                <input type="hidden" name="enablesubtitle" id="subtitlehidden" value="<?= old('enablesubtitle',$slider_data_subtitle)?>">
                            </div>
                            <div class="help-block" style="color: #a94442">
                                {{$errors->first('enablesubtitle')}}
                            </div>
                        </div>
                    </div>
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">Title Position</label>
						<div class="col-md-8">
							<div class="slider-positioner">
								<div class="srow">
									<div class="position">
										<label>
											<input type="radio" name="position" value="top-left" @if($slider_data['position'] == 'top-left') checked @endif>
											<div class="pos-box">TOP LEFT</div>
										</label>
									</div>
									<div class="position">
										<label>
											<input type="radio" name="position" value="top-center"@if($slider_data['position'] == 'top-center') checked @endif>
											<div class="pos-box">TOP CENTER</div>
										</label>
									</div>
									<div class="position">
										<label>
											<input type="radio" name="position" value="top-right"@if($slider_data['position'] == 'top-right') checked @endif>
											<div class="pos-box">TOP RIGHT</div>
										</label>
									</div>
								</div>
								<div class="srow">
									<div class="position">
										<label>
											<input type="radio" name="position" value="center-left"@if($slider_data['position'] == 'center-left') checked @endif>
											<div class="pos-box">CENTER LEFT</div>
										</label>
									</div>
									<div class="position">
										<label>
											<input type="radio" name="position" value="center-center"@if($slider_data['position'] == 'center-center') checked @endif>
											<div class="pos-box">CENTER CENTER</div>
										</label>
									</div>
									<div class="position">
										<label>
											<input type="radio" name="position" value="center-right"@if($slider_data['position'] == 'center-right') checked @endif>
											<div class="pos-box">CENTER RIGHT</div>
										</label>
									</div>
								</div>
								<div class="srow">
									<div class="position">
										<label>
											<input type="radio" name="position" value="bottom-left"@if($slider_data['position'] == 'bottom-left') checked @endif>
											<div class="pos-box">BOTTOM LEFT</div>
										</label>
									</div>
									<div class="position">
										<label>
											<input type="radio" name="position" value="bottom-center"@if($slider_data['position'] == 'bottom-center') checked @endif>
											<div class="pos-box">BOTTOM CENTER</div>
										</label>
									</div>
									<div class="position">
										<label>
											<input type="radio" name="position" value="bottom-right"@if($slider_data['position'] == 'bottom-right') checked @endif>
											<div class="pos-box">BOTTOM RIGHT</div>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
                        	<label class="control-label col-md-2">Description<span class="text-danger"> *</span></label>
                        	<div class="col-md-8 @if($errors->has('description')) has-error @endif">
                            	<?= Form::textarea('description',old('description'),['class'=>'form-control','id'=>'editor1','rows'=>'4']) ?> 
                            	<span class='text-danger error'>{{ $errors->first('description') }}</span>
                        	</div>
                    	</div>
					<div class="form-group">
						<div class="col-sm-2 text-right">
							<?= Form::label('featured image','',['class'=>'control-label'])?><span class="text-danger"> *</span>
						</div>
						<div class="form-group col-sm-8">
							<div class="fileupload fileupload-new col-md-8" data-provides="fileupload">
								<div class="fileupload-new thumbnail img-responsive" style="width: 200px;height: 200px;">
									@if(empty($slider_data['image']))
									<img src="http://via.placeholder.com/200x200" alt="" style="width:200%;height:200%" />
									@else
									<img src="<?=asset('/images/slider/'.$slider_data['image'])?>" style="width:200%;height:200%" />
									@endif
								</div>
								<div class="fileupload-preview fileupload-exists thumbnail " style="width: 200px; line-height: 20px;">
								</div>
								<div>
									<label class="btn btn-file control-label btn-xs">
										<?=Form::file('image', ['class' => 'control-label col-md-2'])?>
										<span id="" class="fileupload-new btn-lg"><i class="fa fa-paper-clip"></i>Select Image</span>
										<span id="" class="fileupload-exists btn-lg"><i class="fa fa-undo"></i>Change</span>
									</label>
									<a href="" class="btn btn-danger fileupload-exists btn-md" data-dismiss="fileupload"><i class="fa fa-trash"></i>Remove</a>
									<br/><span class='text-danger error'>{{ $errors->first('image') }}</span>
								</div>
								<strong><small id="passwordHelpBlock" class="form-text text-muted">
									The image dimension must be 200x200.
								</small></strong>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2 text-right">
							<?= Form::label('Link','',['class'=>'control-label'])?>
						</div>
						<div class="col-sm-8  @if($errors->has('link')) has-error @endif">
							<?= Form::text('link',null,['class'=>'form-control col-sm-2','id'=>'link','placeholder'=>'Enter Link','maxlength'=>'50'])?>
							<div class="help-block">
								{{$errors->first('link')}}
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<div class="form-group">
						<?= Form::label('','',['class'=>'col-sm-2 control-label'])?>
						<div class="col-sm-8 text-right">
							<?= Form::submit('Save',['class'=>'btn btn-primary btn-sm','name'=>'save','title'=>'Save Record'])?>
							<?= Form::submit('Save & Exit',['class'=>'btn btn-primary btn-sm','title'=>'Save & Exit'])?>
							<a href="<?=URL::route('slider.index')?>" title="Back"><button type="button" class="btn btn-default btn-sm" name="cancel">Back</button></a>
						</div>
					</div>
				</div>
				<?= Form::close()?>
			</div>
		</div>
</section>
@stop

@section('css')
<style type="text/css">
    #radioBtn .notActive{
        color: #3276b1;
        background-color: #fff;
    }
    #radioBtn1 .notActive{
        color: #3276b1;
        background-color: #fff;
    }
</style>
@stop

@section('js')
{{ Html::script("http://cdn.ckeditor.com/4.7.1/standard-all/ckeditor.js") }}
<script type="text/javascript">
	$('#radioBtn a').on('click', function(){
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#'+tog).prop('value', sel);
      	$('#titlehidden').attr('value',sel);
        
        $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    });
    $('#radioBtn1 a').on('click', function(){
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#'+tog).prop('value', sel);
       $('#subtitlehidden').attr('value',sel);
        
        $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    });
    var data = "{{old('enabletitle')}}";
    if(data == 'enable')
    {
        $("#title_enable").addClass('active');
        $("#title_enable").removeClass('notActive');
        $("#title_disable").addClass('notActive');
        $("#title_disable").removeClass('active');
    }
    else if(data == 'disable')
    { 
        $("#title_disable").addClass('active');
    }
    var data1 = "{{old('enablesubtitle')}}";
    if(data1 == 'enable')
    {
        $("#subtitle_enable").addClass('active');
        $("#subtitle_enable").removeClass('notActive');
        $("#subtitle_disable").addClass('notActive');
        $("#subtitle_disable").removeClass('active');
    }
    else if(data1 == 'disable')
    { 
        $("#subtitle_disable").addClass('active');
    }

    CKEDITOR.config.extraPlugins = 'justify';
    CKEDITOR.replace( 'editor1', {
        height: 300,

    // Configure your file manager integration. This example uses CKFinder 3 for PHP.
    filebrowserImageBrowseUrl: '/admin/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/admin/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/admin/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/admin/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}',


    toolbar: [
    { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
    { name: 'paragraph',  items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
    { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
    // { name: 'alignment', items : [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
    { name: 'clipboard',items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo']},
    '/',
    { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
    { name : 'links',items : ['Link','Unlink','Anchor']},
    { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
    { name: 'document', items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
    { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'others', items: [ '-' ] },
    ],
    removeButtons: '',
    image_previewText: ' '
});
</script>
@include('admin.layout.alert')

@stop