@extends('admin.layout.index')
@section('content')
<section class="content">
		<div class="col-md-12">
			<div class="box">
		    <div class="box-header with-border">
				<h3 class="box-title">Create Slider</h3>
			</div>
			<br>
			<?= Form::open(['method'=>'post','route'=>'slider.store','class'=>'form-horizontal','enctype'=>'multipart/form-data','files' => 'true'])?>
				<div class="box-body">
					<div class="form-group">
						<div class="col-sm-2 text-right">
							<?= Form::label('title','',['class'=>'control-label'])?><span class="text-danger"> *</span>
						</div>
						<div class="col-sm-8  @if($errors->has('title')) has-error @endif">
							<?= Form::text('title','',['class'=>'form-control','id'=>'title','placeholder'=>'Enter Title','maxlength'=>'50'])?>
							<div class="help-block">
		                        {{$errors->first('title')}}
		                    </div>
						</div>
					</div>
					<div class="form-group">
		                <label for="fun" class="col-sm-2 control-label">Enable/Disable Title</label>
		                    <div class="col-md-8">
		                        <div class="input-group">
		                            <div id="radioBtn" class="btn-group">
		                                <a class="btn btn-primary btn-sm active" data-toggle="happy" data-title="enable" id="title_enable">Enable</a>
		                                <a class="btn btn-primary btn-sm notActive" data-toggle="happy" data-title="disable" id="title_disable">Disable</a>
		                            </div>
		                            <input type="hidden" name="enabletitle" id="titlehidden" value="">
		                        </div>
		                        <span class='text-danger error'>{{ $errors->first('enabletitle') }}</span>
		                    </div>
		                </div>
		                <div class="form-group">
							<div class="col-sm-2 text-right">
								<?= Form::label('sub Title','',['class'=>'control-label'])?><span class="text-danger"> *</span>
							</div>
							<div class="col-sm-8  @if($errors->has('subtitle')) has-error @endif">
								<?= Form::text('subtitle','',['class'=>'form-control','id'=>'sbtitle','placeholder'=>'Enter Sub Title','maxlength'=>'100'])?>
								<div class="help-block">
									{{$errors->first('subtitle')}}
								</div>
							</div>
						</div>
						<div class="form-group">
		                    <label for="fun" class="col-sm-2 control-label">Enable/Disable Sub Title</label>
		                    <div class="col-md-8">
		                        <div class="input-group">
		                            <div id="radioBtn1" class="btn-group">
		                                <a class="btn btn-primary btn-sm active" data-toggle="happy1" data-title="enable" id="subtitle_enable" name="enabletitle">Enable</a>
		                                <a class="btn btn-primary btn-sm notActive" data-toggle="happy1" data-title="disable" name="enabletitle" id="subtitle_disable">Disable</a>
		                            </div>
		                            <input type="hidden" name="enablesubtitle" id="subtitlehidden" value="<?= old('enablesubtitle')?>">
		                        </div>
		                        <div class="help-block" style="color: #a94442">
		                            {{$errors->first('enablesubtitle')}}
		                        </div>
		                    </div>
		                </div>
		                <div class="form-group">
							<label for="name" class="col-sm-2 control-label">Title Position</label>
							<div class="col-md-8">
								<div class="slider-positioner">
									<div class="srow">
										<div class="position" id="all_position">
											<label>
												<input type="radio" name="position" value="top-left">
												<div class="pos-box">TOP LEFT</div>
											</label>
										</div>
										<div class="position">
											<label>
												<input type="radio" name="position" value="top-center">
												<div class="pos-box">TOP CENTER</div>
											</label>
										</div>
										<div class="position">
											<label>
												<input type="radio" name="position" value="top-right">
												<div class="pos-box">TOP RIGHT</div>
											</label>
										</div>
									</div>
									<div class="srow">
										<div class="position">
											<label>
												<input type="radio" name="position" value="center-left">
												<div class="pos-box">CENTER LEFT</div>
											</label>
										</div>
										<div class="position">
											<label>
												<input type="radio" name="position" value="center-center">
												<div class="pos-box">CENTER CENTER</div>
											</label>
										</div>
										<div class="position">
											<label>
												<input type="radio" name="position"value="center-right">
												<div class="pos-box">CENTER RIGHT</div>
											</label>
										</div>
									</div>
									<div class="srow">
										<div class="position">
											<label>
												<input type="radio" name="position" value="bottom-left">
												<div class="pos-box">BOTTOM LEFT</div>
											</label>
										</div>
										<div class="position">
											<label>
												<input type="radio" name="position" value="bottom-center">
												<div class="pos-box">BOTTOM CENTER</div>
											</label>
										</div>
										<div class="position">
											<label>
												<input type="radio" name="position" value="bottom-right">
												<div class="pos-box">BOTTOM RIGHT</div>
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
                        	<label class="control-label col-md-2">Description<span class="text-danger"> *</span></label>
                        	<div class="col-md-8 @if($errors->has('description')) has-error @endif">
                            	<?= Form::textarea('description',old('description'),['class'=>'form-control','id'=>'editor1','rows'=>'4']) ?> 
                            	<span class='text-danger error'>{{ $errors->first('description') }}</span>
                        	</div>
                    	</div>
						<div class="form-group">
							<div class="col-sm-2 text-right">
								<?= Form::label('featured image','',['class'=>'control-label'])?><span class="text-danger"> *</span>
							</div>
							<div class="form-group col-sm-6">
								<div class="fileupload fileupload-new col-md-8" data-provides="fileupload">
									<div class="fileupload-new thumbnail img-responsive" style="width: 200px;height: 200px;">
										<img src="http://via.placeholder.com/200x200" alt="" style="width:200%;height:200%" />
									</div>
									<div class="fileupload-preview fileupload-exists thumbnail " style="width: 200px; line-height: 20px;">
									</div>
									<div>
										<label class="btn btn-file control-label btn-xs">
											<?=Form::file('image', ['class' => 'control-label col-md-2'])?>

											<span id="" class="fileupload-new btn-lg"><i class="fa fa-paper-clip"></i>Select Image</span>
											<span id="" class="fileupload-exists btn-lg"><i class="fa fa-undo"></i>Change</span>
										</label>
										<a href="" class="btn btn-danger fileupload-exists btn-md" data-dismiss="fileupload"><i class="fa fa-trash"></i>Remove</a>
										<br/><span class='text-danger error'>{{ $errors->first('image') }}</span>
										</div>
										<strong><small id="passwordHelpBlock" class="form-text text-muted">
												The image dimension must be 200x200.
										</small></strong>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
                        	<label class="control-label col-md-2">Link</label>
                        	<div class="col-md-8 @if($errors->has('link')) has-error @endif">
                            	<?= Form::text('link',old('link'),['class'=>'form-control','id'=>'link','placeholder'=>'Enter Link']) ?> 
                            	<span class='text-danger error'>{{ $errors->first('link') }}</span>
                        	</div>
                 		</div>	
		                <div class="box-footer">
						<div class="form-group">
							<?= Form::label('','',['class'=>'col-sm-2 control-label'])?>
							<div class="col-sm-8 text-right">
								<button class="btn btn-primary" title="save" type="submit" name="save" value="add">Save</button>
								<button class="btn btn-primary" title="save & new" type="submi" name="save" value="new">Save & Create New Record</button>
								<a href="<?=URL::route('slider.index')?>" title='Back'><button type="button" class="btn btn-default btn-sm" name="cancel">Back</button></a>
							</div>
						</div>       
					</div>
				<?= Form::close()?>
			</div>
		</div>
</section>
@stop

@section('css')
<style type="text/css">
    #radioBtn .notActive{
        color: #3276b1;
        background-color: #fff;
    }
    #radioBtn1 .notActive{
        color: #3276b1;
        background-color: #fff;
    }
</style>
@stop
@section('js')	
{{ Html::script("http://cdn.ckeditor.com/4.7.1/standard-all/ckeditor.js") }}
<script type="text/javascript">
	$('#radioBtn a').on('click', function(){
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#'+tog).prop('value', sel);
        $('#titlehidden').attr('value',sel);
        
        $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    });
    $('#radioBtn1 a').on('click', function(){
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#'+tog).prop('value', sel);
        $('#subtitlehidden').attr('value',sel);
        
        $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
    });
    var data = "{{old('enabletitle')}}";
    if(data == 'enable')
    {
        $("#title_enable").addClass('active');
        $("#title_enable").removeClass('notActive');
        $("#title_disable").addClass('notActive');
        $("#title_disable").removeClass('active');
    }
    else if(data == 'disable')
    { 
        $("#title_disable").addClass('active');
    }
    var data1 = "{{old('enablesubtitle')}}";
    if(data1 == 'enable')
    {
        $("#subtitle_enable").addClass('active');
        $("#subtitle_enable").removeClass('notActive');
        $("#subtitle_disable").addClass('notActive');
        $("#subtitle_disable").removeClass('active');
    }
    else if(data1 == 'disable')
    { 
        $("#subtitle_disable").addClass('active');
    }

    $('#tbName').on('input change', function () {
            if ($(this).val() != '') {
                $('#submit').prop('disabled', false);
            }
            else {
                $('#submit').prop('disabled', true);
            }
        });

    //ckeditor...............
    CKEDITOR.config.extraPlugins = 'justify';
    CKEDITOR.replace( 'editor1', {
        height: 300,

    // Configure your file manager integration. This example uses CKFinder 3 for PHP.
    filebrowserImageBrowseUrl: '/admin/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/admin/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/admin/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/admin/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}',


    toolbar: [
    { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
    { name: 'paragraph',  items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
    { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
    // { name: 'alignment', items : [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
    { name: 'clipboard',items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo']},
    '/',
    { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
    { name : 'links',items : ['Link','Unlink','Anchor']},
    { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
    { name: 'document', items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
    { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'others', items: [ '-' ] },
    ],
    removeButtons: '',
    image_previewText: ' '
});
</script>
@include('admin.layout.alert')

@stop
