@extends('admin.layout.index')
@section('content')
<section class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
			    <div class="box-header with-border">
					<h3 class="box-title">Create Testimonial</h3>
				</div>
				<br>
				<?= Form::open(['method'=>'post','route'=>'testimonial.store','class'=>'form-horizontal','enctype'=>'multipart/form-data','files' => 'true'])?>
					<div class="box-body">
						<div class="form-group">
							<div class="col-sm-2 text-right">
								<?= Form::label('Client Name','',['class'=>'control-label'])?><span class="text-danger"> *</span>
							</div>
							<div class="col-sm-8  @if($errors->has('name')) has-error @endif">
								<?= Form::text('name','',['class'=>'form-control','id'=>'name','placeholder'=>'Enter Client Name','maxlength'=>'30'])?>
								<span class='text-danger error'>
  										{{ $errors->first('name') }}				
								</span>
							</div>
						</div>
							<div class="form-group">
                            	<label class="control-label col-md-2">Description<span class="text-danger"> *</span></label>
                            	<div class="col-md-8 @if($errors->has('description')) has-error @endif">
                                	<?= Form::textarea('description',old('description'),['class'=>'form-control','id'=>'editor1','rows'=>'4']) ?> 
                                	<span class='text-danger error'>{{ $errors->first('description') }}</span>
                            	</div>
                        	</div>
							<div class="form-group">
							<div class="col-sm-2 text-right">
								<?= Form::label('Client Profile','',['class'=>'control-label'])?><span class="text-danger"> *</span>
							</div>
							<div class="form-group col-md-8">
								<div class="fileupload fileupload-new col-md-5" data-provides="fileupload">
									<div class="fileupload-new thumbnail img-responsive" style="width: 100px">
										<img src="http://via.placeholder.com/100x100" alt="" style="width:100%;height:100%" />
									</div>
									<div class="fileupload-preview fileupload-exists thumbnail " style="width: 100px;  line-height: 20px;">
									</div>
									<div>
										<label class="btn btn-file control-label btn-xs">
											<?=Form::file('image', ['class' => 'control-label col-md-2'])?>
											<span id="" class="fileupload-new btn-lg"><i class="fa fa-paper-clip"></i>Select Image</span>
											<span id="" class="fileupload-exists btn-lg"><i class="fa fa-undo"></i>Change</span>
										</label>
										<a href="" class="btn btn-danger fileupload-exists btn-md" data-dismiss="fileupload"><i class="fa fa-trash"></i>Remove</a>
										<br/><span class='text-danger error'>{{ $errors->first('image') }}</span>
									</div>
									<small id="passwordHelpBlock" class="form-text text-muted">
										The image dimension must be 100x100.
									</small>
								</div>
							</div>
						</div>
					</div>
			                <div class="box-footer">
							<div class="form-group">
								<?= Form::label('','',['class'=>'col-sm-2 control-label'])?>
								<div class="col-sm-8 text-right">
									<button class="btn btn-primary" title="save" type="submit" name="save" value="add">Save</button>
									<button class="btn btn-primary" title="save & new" type="submi" name="save" value="new">Save & Create New Record</button>
									<a href="<?=URL::route('testimonial.index')?>" title='Back'><button type="button" class="btn btn-default btn-sm" name="cancel">Back</button></a>
								</div>
							</div>       
						</div>
					<?= Form::close()?>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('css')
@stop
@section('js')	
{{ Html::script("http://cdn.ckeditor.com/4.7.1/standard-all/ckeditor.js") }}
<script type="text/javascript">
    CKEDITOR.config.extraPlugins = 'justify';
    CKEDITOR.replace( 'editor1', {
        height: 300,

    // Configure your file manager integration. This example uses CKFinder 3 for PHP.
    filebrowserImageBrowseUrl: '/admin/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/admin/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/admin/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/admin/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}',


    toolbar: [
    { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
    { name: 'paragraph',  items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
    { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
    // { name: 'alignment', items : [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
    { name: 'clipboard',items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo']},
    '/',
    { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
    { name : 'links',items : ['Link','Unlink','Anchor']},
    { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
    { name: 'document', items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
    { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'others', items: [ '-' ] },
    ],
    removeButtons: '',
    image_previewText: ' '
});
</script>
@include('admin.layout.alert')

@stop
