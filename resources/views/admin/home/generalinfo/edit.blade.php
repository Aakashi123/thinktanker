@extends('admin.layout.index')
@section('content')
<section class="content">
  <h3>General Settings</h3>
    <?= Form::model($generalinfo_data,['route'=>['generalinfo.update','id'=>$generalinfo_data->id],'method'=>'PATCH','files'=>'true'])?>
    <div class="text-right">
      <?=Form::submit('Save',['title' => 'save','class'=>'btn btn-primary btn-sm','style'=>'margin-top:-65px'])?>
    </div>
    <div class="box blue-border">
        <div class="box-header with-border">
        <h3 class="box-title">Social Media Link</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= Form::label('Google +')?>
                        <div class="@if($errors->has('google')) has-error @endif">
                            <?= Form::text('google',null,['class'=>'form-control','id'=>'google','placeholder'=>'Enter Google+ Link'])?>
                            <div class="help-block">
                                {{$errors->first('google')}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Form::label('Linkedin')?>
                        <div class="@if($errors->has('linkedin')) has-error @endif">
                            <?= Form::text('linkedin',null,['class'=>'form-control','id'=>'linkedin','placeholder'=>'Enter Linkedin Link'])?>
                            <div class="help-block">
                                {{$errors->first('linkedin')}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                    <?= Form::label('Instagram')?>
                    <div class="@if($errors->has('instagram')) has-error @endif">
                        <?= Form::text('instagram',null,['class'=>'form-control','id'=>'instagram','placeholder'=>'Enter Instagram Link'])?>
                        <div class="help-block">
                            {{$errors->first('pinterest')}}
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= Form::label('Facebook')?>
                        <div class="@if($errors->has('facebook')) has-error @endif">
                            <?= Form::text('facebook',null,['class'=>'form-control','id'=>'facebook','placeholder'=>'Enter Facebook Link'])?>
                            <div class="help-block">
                                {{$errors->first('facebook')}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Form::label('Twitter')?>
                        <div class="@if($errors->has('twitter')) has-error @endif">
                            <?= Form::text('twitter',null,['class'=>'form-control','id'=>'twitter','placeholder'=>'Enter Twitter Link'])?>
                            <div class="help-block">
                                {{$errors->first('twitter')}}
                            </div>
                        </div>  
                    </div>
                    <div class="form-group">
                        <?= Form::label('Youtube')?>
                        <div class="@if($errors->has('youtube')) has-error @endif">
                            <?= Form::text('youtube',null,['class'=>'form-control','id'=>'youtube','placeholder'=>'Enter Youtube Link'])?>
                            <div class="help-block">
                                {{$errors->first('youtube')}}
                            </div>
                        </div>
                    </div>  
              </div>
            </div>
        </div>
    </div>
    <div class="box blue-border">
        <div class="box-header with-border">
            <h3 class="box-title">Contact Us</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= Form::label('Title')?><span class="text-danger"> *</span>
                            <div class="@if($errors->has('title')) has-error @endif">
                                <?= Form::text('title',null,['class'=>'form-control','id'=>'title','placeholder'=>'Enter Title','maxlength'=>'50'])?>
                                <div class="help-block">
                                    {{$errors->first('title')}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= Form::label('SubTitle')?>
                            <div class="@if($errors->has('subtitle')) has-error @endif">
                                <?= Form::text('subtitle',null,['class'=>'form-control','id'=>'subtitle','placeholder'=>'Enter SubTitle','maxlength'=>'100'])?>
                                <div class="help-block">
                                    {{$errors->first('subtitle')}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= Form::label('Email Id')?><span class="text-danger"> *</span>
                            <div class="@if($errors->has('email')) has-error @endif">
                                <?= Form::text('email',null,['class'=>'form-control','id'=>'email','placeholder'=>'Enter Email Id'])?>
                                <div class="help-block">
                                    {{$errors->first('email')}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= Form::label('ContactNo')?><span class="text-danger"> *</span>
                            <div class="@if($errors->has('mobile1')) has-error @endif">
                                <?= Form::text('mobile1',null,['class'=>'form-control number_only','id'=>'mobile1','placeholder'=>'Enter MobileNo1'])?>
                                <div class="help-block">
                                    {{$errors->first('mobile1')}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= Form::label('Timing Detail')?><span class="text-danger"> *</span>
                            <div class="@if($errors->has('time')) has-error @endif">
                            <?= Form::text('time',null,['class'=>'form-control','id'=>'time','placeholder'=>'Enter Timing Detail'])?>
                                <div class="help-block">
                                  {{$errors->first('time')}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= Form::label('Alternate ContactNo')?><span class="text-danger"> *</span>
                            <div class="@if($errors->has('mobile2')) has-error @endif">
                                <?= Form::text('mobile2',null,['class'=>'form-control number_only','id'=>'mobile2','placeholder'=>'Enter MobileNo2'])?>
                                <div class="help-block">
                                    {{$errors->first('mobile2')}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= Form::label('Ahmedabad Address')?><span class="text-danger"> *</span>
                            <div class="@if($errors->has('address')) has-error @endif">
                                <?= Form::textarea('address',null,['rows'=>'5','cols'=>'5','class'=>'form-control','id'=>'address','placeholder'=>'Enter Ahmedabad Address','maxlength'=>'100'])?>
                                <div class="help-block">
                                    {{$errors->first('address')}}
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <?= Form::label('Mumbai Address')?><span class="text-danger"> *</span>
                            <div class="@if($errors->has('address1')) has-error @endif">
                                <?= Form::textarea('address1',null,['rows'=>'5','cols'=>'5','class'=>'form-control','id'=>'address','placeholder'=>'Enter Mumbai Address','maxlength'=>'100'])?>
                                <div class="help-block">
                                    {{$errors->first('address1')}}
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
    <div class="box blue-border">
        <div class="box-header with-border">
          <h3 class="box-title">Other Details</h3>
        </div>
        <div class="box-body">
          <div class="form-group">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <?= Form::label('meta title')?>
                  <div class="@if($errors->has('meta_title')) has-error @endif">
                    <?= Form::text('meta_title',null,['class'=>'form-control','id'=>'Meta Title','placeholder'=>'Enter Meta Title'])?>
                    <div class="help-block">
                      {{$errors->first('meta_title')}}
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <?= Form::label('meta description')?>
                  <div class="@if($errors->has('meta_description')) has-error @endif">
                    <?= Form::textarea('meta_description',null,['class'=>'form-control','id'=>'Meta Description','placeholder'=>'Enter Meta Description','rows'=>'5'])?>
                    <div class="help-block">
                      {{$errors->first('meta_description')}}
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <?= Form::label('Copyright')?><span class="text-danger"> *</span>
                  <div class="@if($errors->has('copyright')) has-error @endif">
                    <?= Form::text('copyright',null,['class'=>'form-control','id'=>'copyright','placeholder'=>'Enter Copyright'])?>
                    <div class="help-block">
                      {{$errors->first('copyright')}}
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <?= Form::label('meta keyword')?>
                  <div class="@if($errors->has('meta_keyword')) has-error @endif">
                    <?= Form::textarea('meta_keyword',null,['class'=>'form-control','id'=>'Meta Description','placeholder'=>'Enter Meta Keyword','rows'=>'5'])?>
                    <div class="help-block">
                      {{$errors->first('meta_keyword')}}
                    </div>
                  </div>
                </div>
              </div> 
            </div>
          </div>
        </div>
    </div>    
    <div class="text-right">
      <?=Form::submit('Save',['name' => 'save','title' => 'save','class'=>'btn btn-primary btn-sm'])?>
    </div>
    <br>
    <?=Form::close()?>
  </div>
</section>
@stop
@section('js')
<script type="text/javascript">
    $(".number_only").keypress(function(h){if(8!=h.which&&0!=h.which&&(h.which<48||h.which>57))return!1});
</script>
  @include('admin.layout.alert')
@stop

