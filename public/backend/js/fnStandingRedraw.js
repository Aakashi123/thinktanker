jQuery.fn.dataTableExt.oApi.fnStandingRedraw=function(oSettings){if(oSettings.oFeatures.bServerSide===!1){var before=oSettings._iDisplayStart;oSettings.oApi._fnReDraw(oSettings);oSettings._iDisplayStart=before;oSettings.oApi._fnCalculateEnd(oSettings)}
oSettings.oApi._fnDraw(oSettings)};jQuery.fn.dataTableExt.oApi.fnFilterClear=function(oSettings)
{var i,iLen;oSettings.oPreviousSearch.sSearch="";if(typeof oSettings.aanFeatures.f!='undefined')
{var n=oSettings.aanFeatures.f;for(i=0,iLen=n.length;i<iLen;i++)
{$('input',n[i]).val('')}}
for(i=0,iLen=oSettings.aoPreSearchCols.length;i<iLen;i++)
{oSettings.aoPreSearchCols[i].sSearch=""}
oSettings.oApi._fnReDraw(oSettings)};jQuery.fn.dataTableExt.oApi.fnSetFilteringDelay=function(oSettings,iDelay){var _that=this;if(iDelay===undefined){iDelay=250}
this.each(function(i){$.fn.dataTableExt.iApiIndex=i;var oTimerId=null,sPreviousSearch=null,anControl=$('input',_that.fnSettings().aanFeatures.f);anControl.unbind('keyup search input').bind('keyup search input',function(){if(sPreviousSearch===null||sPreviousSearch!=anControl.val()){window.clearTimeout(oTimerId);sPreviousSearch=anControl.val();oTimerId=window.setTimeout(function(){$.fn.dataTableExt.iApiIndex=i;_that.fnFilter(anControl.val())},iDelay)}});return this});return this}