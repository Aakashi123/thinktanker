<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::get('admin/home','AdminController@index');
Route::get('admin','Admin\LoginController@showLoginForm')->name('admin.login');
Route::post('admin','Admin\LoginController@login');
Route::post('admin-password/email','Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('admin-password/reset','Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('admin-password/reset','Admin\ResetPasswordController@reset');
Route::get('admin-password/reset/{token}','Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
Route::post('admin/logout','Admin\LoginController@logout')->name('admin.logout');

Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware'=>['admin']],function(){

	//HOMEPAGE MENU ROUTES
	Route::resource('/slider','SliderController',['except'=>'destroy']);
	Route::post('/slider/delete',[
		'uses'	=> 'SliderController@delete',
		'as'	=> 'slider.delete',
	]);

	Route::post('/slider/order','SliderController@order')->name('slider.order');

	Route::resource('/testimonial','TestimonialController',['except'=>'destroy']);
	Route::post('/testimonial/delete',[
		'uses'	=> 'TestimonialController@delete',
		'as'	=> 'testimonial.delete',
	]);

	Route::resource('/generalinfo','GeneralInfoController');

	//MASTER MENU ROUTES

	Route::resource('/projectmaster','ProjectMasterController',['except'=>'destroy']);
	Route::post('/projectmaster/delete',[
		'uses'	=> 'ProjectMasterController@delete',
		'as'	=> 'projectmaster.delete',
	]);

	Route::resource('/cms','CmsController',['except'=>'destroy']);
	Route::post('/cms/delete',[
		'uses'	=> 'CmsController@delete',
		'as'	=> 'cms.delete',
	]);

	Route::resource('/career','CareerController',['except'=>'destroy']);
	Route::post('/career/delete',[
		'uses'	=> 'CareerController@delete',
		'as'	=> 'career.delete',
	]);

	//SERVICES MENU ROUTES

	Route::resource('/servicemaster','ServiceMasterController',['except'=>'destroy']);
	Route::post('/servicemaster/delete',[
		'uses'	=> 'ServiceMasterController@delete',
		'as'	=> 'servicemaster.delete',
	]);

	Route::resource('/servicepart','ServiceInnerPartController',['except'=>'destroy']);
	Route::post('/servicepart/delete',[
		'uses'	=> 'ServiceInnerPartController@delete',
		'as'	=> 'servicepart.delete',
	]);


	//CLIENT LOGO ROUTES

	Route::resource('/clientlogo','ClientLogoController',['except'=>['destroy','edit','update','show']]);
	Route::get('/clientlogo/edit',[
		'uses' => 'ClientLogoController@edit',
		'as'	=> 'clientlogo.edit',
	]);
	Route::post('/clientlogo/update',[
		'uses' => 'ClientLogoController@update',
		'as'	=> 'clientlogo.update',
	]);
	Route::post('/clientlogo/delete',[
		'uses'	=> 'ClientLogoController@delete',
		'as'	=> 'clientlogo.delete',
	]);

	//MENU ROUTES
	Route::get('menu','Themecontroller@index')->name('menu.get');

	//File Manager Routes
	Route::get('/laravel-filemanager', '\Unisharp\Laravelfilemanager\controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\Unisharp\Laravelfilemanager\controllers\UploadController@upload');

});


Route::group(['middleware' => 'set_active_menu'], function(){
	Route::get('/','Frontend\HomeController@index')->name('homepage.index');
	Route::get('/{slug}','Frontend\DetailsController@details')->name('details.get');
});